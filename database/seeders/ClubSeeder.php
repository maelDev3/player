<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\ClubTeam;

use Illuminate\Support\Facades\DB;

class ClubSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        
        DB::table('club_teams')->insert([
            "name"=>'Club Liverpol '
        ]);

        DB::table('club_teams')->insert([
            "name"=>'PSG'
        ]);

        DB::table('club_teams')->insert([
            "name"=>'Real Madride'
        ]);

        DB::table('club_teams')->insert([
            "name"=>'Barea'
        ]);
    }
}
