<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Schema::disableForeignKeyConstraints();
		// DB::table('users')->truncate();

        // User::create([
        //     'nom'=>'mael',
        //     'prenom'=>'dev',
        //     'email'=>'maeldev3@gmail.com',
        //     'password'=> Hash::make('admin'),
        //     'is_admin'=>true,
        //     'is_valide'=>true
        // ]);

        // User::create([
        //     'nom'=>'mael',
        //     'prenom'=>'rbm',
        //     'email'=>'maelrbm3@gmail.com',
        //     'password'=> Hash::make('mael'),
        //     'is_admin'=>false,
        //     'is_valide'=>false
        // ]);
        // Schema::enableForeignKeyConstraints();
         // Création d'un utilisateur admin
         User::create([
            'name' => 'Admin User',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('password'),
            'role' => 'admin', // Si un champ 'role' existe
            'remember_token' => Str::random(10),
        ]);

        // Création d'un utilisateur classique
        User::create([
            'name' => 'Mael dev',
            'email' => 'maeldev3@gmail.com',
            'password' => Hash::make('password'),
            'role' => 'user', // Si un champ 'role' existe
            'remember_token' => Str::random(10),
        ]);

        // Ajout de plusieurs utilisateurs manuels
        $users = [
            [
                'name' => 'Alice Smith',
                'email' => 'alice@gmail.com',
                'password' => Hash::make('password'),
                'role' => 'user',
            ],
            [
                'name' => 'Bob Johnson',
                'email' => 'bob@gmail.com',
                'password' => Hash::make('password'),
                'role' => 'user',
            ]
        ];

        foreach ($users as $user) {
            User::create($user);
        }
    }
    
}
