<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Service;

use Illuminate\Support\Facades\DB;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('services')->insert([
            [
                'domicile_aeroport' => '123 Main Street',
                'domicile_destination' => '456 Elm Street',
                'aeroport_destination' => 'XYZ Airport',
                'courrier' => 'Express',
                'restauration' => 'Gourmet',
                'location_voiture' => 'Luxury Car',
                'reservation_voiture' => 'Yes',
                'bagages' => '2 bags',
                'conciergerie' => 'Full Service',
                'autre' => 'Special Request'
            ],
        ]);

        
        DB::table('services')->insert([
            [
                'domicile_aeroport' => '789 Oak Avenue',
                'domicile_destination' => '101 Pine Street',
                'aeroport_destination' => 'ABC Airport',
                'courrier' => 'Standard',
                'restauration' => 'Vegetarian',
                'location_voiture' => 'SUV',
                'reservation_voiture' => 'No',
                'bagages' => '1 suitcase',
                'conciergerie' => 'Basic',
                'autre' => 'None'
            ],
            // Ajoutez d'autres enregistrements fictifs si nécessaire...
        ]);

    }
}
