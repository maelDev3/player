<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeVoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('type_voles')->insert([
            "type"=>'Vol Business travel'
            
        ]);

        DB::table('type_voles')->insert([
            "type"=>'VOL VIP'
        ]);
    }
}
