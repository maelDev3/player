<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeService extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('type_services')->insert([
            "prix"=>'10',
            "service"=>'Traiter'
        ]);

        DB::table('type_services')->insert([
            "prix"=>'20',
            "service"=>'Freelance'
        ]);
    }
}
