<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\NationalTeam;

class NationalTeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        NationalTeam::create(['name'=>'Malagasy']);
        NationalTeam::create(['name'=>'France']);
        NationalTeam::create(['name'=>'Bilgarie']);
        NationalTeam::create(['name'=>'Italie']);
    }
}
