<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->id();
            $table->string('code_icao_depart')->nullable();
            $table->string('code_icao_arrive')->nullable();
            $table->string('localisation_arrive')->nullable();
            $table->string('localisation_depart')->nullable();
            $table->string('localisation_arrive_retour')->nullable();
            $table->string('localisation_depart_retour')->nullable();
            $table->string('type_de_booking')->nullable();
            $table->string('bagages')->nullable();
            $table->date('date_depart')->nullable();
            $table->date('date_arrive')->nullable();
            $table->boolean('escale')->nullable();
            $table->string('by_user')->nullable();
            $table->string('heure_depart_aller')->nullable();
            $table->string('date_depart_aller')->nullable();
            $table->string('heure_depart_retour')->nullable();
            $table->string('date_depart_retour')->nullable();
            $table->string('nbre_voyageur')->nullable();
            $table->string('billet')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('reservations');
    }
};
