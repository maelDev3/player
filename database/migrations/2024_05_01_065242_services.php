<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('domicile_aeroport')->nullable();
            $table->string('domicile_destination')->nullable();
            $table->string('aeroport_destination')->nullable();
            $table->string('courrier')->nullable();
            $table->string('restauration')->nullable();
            $table->string('location_voiture')->nullable();
            $table->string('reservation_voiture')->nullable();
            $table->string('bagages')->nullable();
            $table->string('conciergerie')->nullable();
            $table->string('autre')->nullable();
            $table->string('prix')->nullable();
            $table->timestamps();
        });
    
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('services');
    }
};
