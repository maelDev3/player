<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('airports', function (Blueprint $table) {
            $table->id();
            $table->string('nom')->nullable();
            $table->string('code_icao')->nullable();
            $table->string('code_iata')->nullable();
            $table->string('longeur_piste')->nullable();
            $table->string('latitude_deg')->nullable();
            $table->string('longitude_deg')->nullable();
            $table->string('ifr')->nullable();
            $table->string('eclairage_nuit')->nullable();
            $table->string('acces_piste_voiture')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('airports');
    }
};
