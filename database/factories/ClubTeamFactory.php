<?php

namespace Database\Factories;

use App\Models\ClubTeam;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClubTeamFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ClubTeam::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->company,
            // Add other fields as needed
        ];
    }
}
