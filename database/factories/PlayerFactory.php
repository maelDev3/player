<?php

namespace Database\Factories;

use App\Models\Player;
use Illuminate\Database\Eloquent\Factories\Factory;

class PlayerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Player::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'goals' => $this->faker->numberBetween(0, 50),
            'club_id' => function () {
                return \App\Models\ClubTeam::factory()->create()->id;
            },
            'national_team_id' => function () {
                return \App\Models\NationalTeam::factory()->create()->id;
            },
        ];
    }
}
