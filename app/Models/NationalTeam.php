<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NationalTeam extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function players(){
        return $this->hasMany(Players::class);
    }
}
