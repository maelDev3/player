<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    use HasFactory;
    protected $guarded = [];
    
    public function club(){
        return $this->belongsTo(ClubTeam::class);
    }

    public function nationalTeam(){
        return $this->belongsTo(NationalTeam::class);
    }

}
