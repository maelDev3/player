<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\{Player,ClubTeam,NationalTeam};
use Redirect,Response;

class PlayersController extends Controller
{
    
    public function __invoke(){
        $clubs = ClubTeam::select(['id','name'])->get();
        $nationalteams = NationalTeam::select(['id','name'])->get();
        $players = Player::orderBy('name')->with("nationalTeam","club")->get();
        return view('players.index',compact('players','clubs','nationalteams'));  
    }
    
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $clubs = ClubTeam::select(['id','name'])->get();
        $nationalteams = NationalTeam::select(['id','name'])->get();
        return view('players.create',compact('clubs','nationalteams')); 
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'   => 'required',
        ]);
        $data=[
            'name'=>$request->name,
            'goals'=>$request->goals,
            'club_id'=>$request->club_id,
            'national_team_id'=>$request->national_team_id,
        ];
        $player = Player::create($data);
        $player->load('nationalTeam', 'club');
        return Response::json($player);
    }

  

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $clubs = ClubTeam::select(['id','name'])->get();
        $nationalteams = NationalTeam::select(['id','name'])->get();
        $player = Player::with('club','nationalTeam')->find($id);
        return Response::json($player);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $player = Player::find($id);
        if (!$player) {
            return response()->json(['error' => 'Player not found'], 404);
        }
        $player->name = $request->name;
        $player->goals = $request->goals;
        $player->club_id = $request->club_id;
        $player->national_team_id = $request->national_team_id;
        $player->save();

        $player->load('nationalTeam', 'club');

        return response()->json($player);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $player = Player::where('id',$id)->delete();
        return Response::json($player);
    }

    public function filter(Request $request)
    {
        $clubId = $request->input('club_id');
        $nationalTeamId = $request->input('national_team_id');
        $players = Player::where(function ($query) use ($clubId, $nationalTeamId) {
            if (!empty($clubId)) {
                $query->where('club_id', $clubId);
            }
            if (!empty($nationalTeamId)) {
                $query->where('national_team_id', $nationalTeamId);
            }
        })->with('nationalTeam', 'club')->get();
        return Response::json($players);
    }

}
