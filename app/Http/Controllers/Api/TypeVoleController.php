<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\TypeVole;
use Illuminate\Http\Request;

class TypeVoleController extends Controller
{
    public function index()
    {
        $typevoles = TypeVole::select('id', 'type')->get();
        return response()->json($typevoles);
    }
    public function create(Request $request)
    {
        try {
            $typevole = TypeVole::create(['type' => $request->type]);
            return response()->json([
                'message' => 'created Type Vole succes !',
                'data'   => $typevole
            ]);
        } catch (\Throwable $th) {
            return [
                'message'     => $th->getMessage(),
                'status_code' => 501
            ];
        }
    }
    public function edit($id)
    {
        $typevole = TypeVole::find($id);
        return response()->json($typevole);
    }
    public function update(Request $request, $id)
    {
        try {
            $typevole = TypeVole::find($id);
            $typevole->update(['type' => $request->type]);
            return response()->json([
                'message' => 'updated Type Vole succes !',
                'data'   => $typevole
            ]);
        } catch (\Throwable $th) {
            return [
                'message'     => $th->getMessage(),
                'status_code' => 501
            ];
        }
    }
    public function delete($id)
    {
        if ($id) {
            TypeVole::whereId($id)->delete();
            return response()->json([
                'message' => 'deleted Type Vole succes !',
            ]);
        }
    }
}
