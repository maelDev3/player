<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Airport;

class AirportController extends Controller
{
    public function index()
    {
        $airport = Airport::get();
        return response()->json($airport);
    }
    public function create(Request $request)
    {

        try {
            $airport = Airport::create([
                'nom' => $request['nom'],
                'code_icao' => $request['code_icao'],
                'code_iata' => $request['code_iata'],
                'longeur_piste' => $request['longeur_piste'],
                'latitude_deg' => $request['latitude_deg'],
                'longitude_deg' => $request['longitude_deg'],
                'eclairage_nuit' => $request['eclairage_nuit'],
                'acces_piste_voiture' => $request['acces_piste_voiture'],
                'ifr' => $request['ifr'],

                ]);
            return response()->json([
                'message' => 'created airport succes !',
                'data'   => $airport
            ]);
        } catch (\Throwable $th) {
            return [
                'message'     => $th->getMessage(),
                'status_code' => 501
            ];
        }
    }
    public function edit($id)
    {
        $airport = Airport::find($id);
        return response()->json($airport);
    }
    public function update(Request $request, $id)
    {
        try {
            $airport = Airport::whereId($id)->update([
                'nom' => $request['nom'],
                'code_icao' => $request['code_icao'],
                'code_iata' => $request['code_iata'],
                'longeur_piste' => $request['longeur_piste'],
                'latitude_deg' => $request['latitude_deg'],
                'longitude_deg' => $request['longitude_deg'],
                'eclairage_nuit' => $request['eclairage_nuit'],
                'acces_piste_voiture' => $request['acces_piste_voiture'],
                'ifr' => $request['ifr'],
                ]);
            return response()->json([
                'message' => 'updated mode payement succes !',
                'data'   => $airport
            ]);
        } catch (\Throwable $th) {
            return [
                'message'     => $th->getMessage(),
                'status_code' => 501
            ];
        }
    }
    public function delete($id)
    {
        Airport::whereId($id)->delete();
        return response()->json([
            'message' => 'Deleted mode payement success!'
        ]);
    }
}
