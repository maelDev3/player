<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Service;

class ServiceController extends Controller
{
    public function index()
    {
      $services = Service::select(['id','domicile_aeroport','domicile_destination','aeroport_destination','courrier','restauration','location_voiture','reservation_voiture','bagages','autre','prix'])->get();
      return response()->json($services);

    }
    public function create(Request $request)
    {
        try {
            $data['domicile_aeroport']    = $request->domicile_aeroport;
            $data['domicile_destination'] = $request->domicile_destination;
            $data['aeroport_destination'] = $request->aeroport_destination;
            $data['courrier']             = $request->courrier;
            $data['restauration']         = $request->restauration;
            $data['location_voiture']     = $request->location_voiture;
            $data['reservation_voiture']  = $request->reservation_voiture;
            $data['bagages']              = $request->bagages;
            $data['conciergerie']         = $request->conciergerie;
            $data['autre']                = $request->autre;
            $data['prix']                 = $request->prix;

            $service = Service::create($data);

            return response()->json([
                'message'=>'created service success',
                'data'=>$service
            ]);

        } catch (\Throwable $th) {
            return [
                'message' => $th->getMessage(),
                'status_code' => 501
            ];
        }

    }

    public function edit($id)
    {
        $service = Service::find($id);
        return response()->json($service);
    }


    public function update(Request $request,$id)
    {
        try {
        $service = Service::find($id);
        $data['domicile_aeroport']    = $request->domicile_aeroport;
        $data['domicile_destination'] = $request->domicile_destination;
        $data['aeroport_destination'] = $request->aeroport_destination;
        $data['courrier']             = $request->courrier;
        $data['restauration']         = $request->restauration;
        $data['location_voiture']     = $request->location_voiture;
        $data['reservation_voiture']  = $request->reservation_voiture;
        $data['bagages']              = $request->bagages;
        $data['conciergerie']         = $request->conciergerie;
        $data['autre']                = $request->autre;
        $data['prix']                 = $request->prix;
        $service = $service->update($data);
        return response()->json([
            'message'=>'updated service success',
            'data'=>$service
        ]);
    } catch (\Exception $e) {
        return $e->getmessage();
    }
    }
    public function delete($id)
    {
        if($id)
        {
            $service = Service::whereId($id)->delete();
            return response()->json([
                'message'=>'deleted service success',
                'data'=>$service
            ]);
        }
    }
}
