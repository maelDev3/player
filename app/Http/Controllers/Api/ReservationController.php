<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\{Reservation,Airport};
use App\Models\ReservationService;
use App\Models\ReservationSiege;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    public function index()
    {
        $reservation = Reservation::with([
            'reservationService.service',
            'reservationSiege.siege'
            ])->get();
        return response()->json($reservation);
    }
    public function create(Request $request)
    {
        try {
            $reservation = Reservation::create([
                'code_icao_depart'            => $request['code_icao_depart'],
                'code_icao_arrive'            => $request['code_icao_arrive'],
                'type_de_booking'             => $request['type_de_booking'],
                'bagages'                     => $request['bagages'],
                'date_depart'                 => $request['date_depart'],
                'date_arrive'                 => $request['date_arrive'],
                'escale'                      => $request['escale'] ? $request['escale'] :false,
                // 'by_user'                     => $request['travel_id'] ? "voyageur" : "Auto",
                // 'travel_id'                   => $request['travel_id'],
                // 'plane_id'                    => $request['plane_id'],

                'heure_depart_aller'          => $request['heure_depart_aller'],
                'date_depart_aller'           => $request['date_depart_aller'],
                'heure_depart_retour'         => $request['heure_depart_retour'],
                'date_depart_retour'          => $request['date_depart_retour'],
                'localisation_arrive'         => $request['localisation_arrive'],
                'localisation_depart'         => $request['localisation_depart'],
                'nbre_voyageur'               => $request['nbre_voyageur'],
                'billet'                      => $request['billet'],
                // 'vole_id'                     => $request['vole_id'],
                'localisation_arrive_retour'  => $request['localisation_arrive_retour'],
                'localisation_depart_retour'  => $request['localisation_depart_retour'],

            ]);

            // if($request->arrive_lng_lat && $request->depart_lng_lat)
            //  {
                 $airport = $this->filterLongLat($request);
            //  }

            // if($request['siege_id'])
            // {
            //     foreach($request['siege_id'] as $item)
            //     {
            //         ReservationSiege::create([
            //             'siege_id' => $item,
            //             'reservation_id' => $reservation->id
            //         ]);
            //     }
            // }
            if(count($request['service_id']))
            {
                foreach($request['service_id'] as $item)
                {
                    ReservationService::create([
                        'service_id' => $item,
                        'reservation_id' => $reservation->id
                    ]);
                }
            }


            return response()->json([
                'message' => 'created reservation succes !',
                'data'    => $reservation,
                'airport' =>$airport
            ]);
        } catch (\Throwable $th) {
            return [
                'message'     => $th->getMessage(),
                'status_code' => 501
            ];
        }
    }
    public function edit($id)
    {
        $Reservation = Reservation::with([
            'reservationService.service',
            'reservationSiege.siege'
            ])->find($id);
        return response()->json($Reservation);
    }
    public function update(Request $request, $id)
    {
        try {
            $reservation = Reservation::find($id);
            $reservation->update([
                'code_icao_depart'            => $request['code_icao_depart'],
                'code_icao_arrive'            => $request['code_icao_arrive'],
                'type_de_booking'             => $request['type_de_booking'],
                'bagages'                     => $request['bagages'],
                'date_depart'                 => $request['date_depart'],
                'date_arrive'                 => $request['date_arrive'],
                'escale'                      => $request['escale'] ? $request['escale'] :false,
                'heure_depart_aller'          => $request['heure_depart_aller'],
                'date_depart_aller'           => $request['date_depart_aller'],
                'heure_depart_retour'         => $request['heure_depart_retour'],
                'date_depart_retour'          => $request['date_depart_retour'],
                'localisation_arrive'         => $request['localisation_arrive'],
                'localisation_depart'         => $request['localisation_depart'],
                'nbre_voyageur'               => $request['nbre_voyageur'],
                'billet'                      => $request['billet'],
               
                'localisation_arrive_retour'  => $request['localisation_arrive_retour'],
                'localisation_depart_retour'  => $request['localisation_depart_retour'],

            ]);

            if($request['seige_id'])
            {
                ReservationSiege::where('reservation_id',$id)->delete();
                foreach($request['seige_id'] as $item)
                {
                    $reservation->reservationSiege()->create(['siege_id'=>$item]);
                }
            }
            else{
                ReservationSiege::where('reservation_id',$id)->delete();
            }
            if($request['service_id'])
            {
                ReservationService::where('reservation_id',$id)->delete();
                foreach($request['service_id'] as $item)
                {
                    ReservationService::create([
                        'service_id' => $item,
                        'reservation_id' => $id
                    ]);
                }
            }else{
                ReservationService::where('reservation_id',$id)->delete();
            }
            return response()->json([
                'message' => 'Updated reservation succes !',
                'data'   => $reservation
            ]);
        } catch (\Throwable $th) {
            return [
                'message'     => $th->getMessage(),
                'status_code' => 501
            ];
        }
    }
    public function delete($id)
    {
        Reservation::whereId($id)->delete();
        return response()->json([
            'message' => 'Deleted Reservation success!'
        ]);
    }

    private function filterLongLat($request)
    {
            try{
            $data = [] ;
            $airport =  Airport::all();
           
            if($request->depart_lng_lat){
                $coords = $request->depart_lng_lat['features']['geometry']['coordinates'];
                foreach ($airport as $item) {
                    // $dist = getDistanceBetweenPoints((float) $item->latitude_deg,(float) $item->longitude_deg,43.620321,3.8036822);
                    $dist = getDistanceBetweenPoints((float) $item->latitude_deg,(float) $item->longitude_deg,$coords[1],$coords[0]);
                    if($dist <= 150000){
                        $data['depart'][] = $item ;
                    }
                }
            }

            if($request->arrive_lng_lat){
                $coords = $request->arrive_lng_lat['features']['geometry']['coordinates'];
                foreach ($airport as $item) {
                    $dist = getDistanceBetweenPoints((float) $item->latitude_deg,(float) $item->longitude_deg,$coords[1],$coords[0]);
                    if($dist <= 150000){
                        $data['arrive'][] = $item ;
                    }
                }
            }
            return $data;

            } catch (\Throwable $th) {
                return [
                    'message'     => $th->getMessage(),
                    'status_code' => 501
                ];
            }
            }

}
