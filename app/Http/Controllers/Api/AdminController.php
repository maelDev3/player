<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Mail\ForgotPasswordMail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Crypt;

class AdminController extends Controller
{
    public function authentificate(Request $request)
    {
        try {
            $user = User::whereEmail($request->email)->first();
            if (!$user) {
                return [
                    'error' => 'User not exist',
                    'status_code' => 404
                ];
            }
            if ($user->password) {
                if (!Hash::check($request->password, $user->password)) {
                    return [
                        'error' => 'Credentials wrong',
                        'message' => 'Credentials wrong',
                        'status_code' => 401
                    ];
                }
            }
            return response()->json([
                'token' => $user->createToken(time())->plainTextToken,
                'user' => $user,
            ]);
        } catch (Exception $e) {
            return [
                'error' => 'something_went_wrong',
                'message' => $e->getMessage(),
                'status_code' => 500
            ];
        }
    }

    public function forgotPasword(Request $request)
    {
        try {
            $user = User::whereEmail($request['email'])->first();
            if (!$user) {
                return [
                    'error' => 'User not exist',
                    'status_code' => 404
                ];
            }

            if (isset($user)) {
                $request->validate(['email' => 'required|email']);
                // $status = Password::sendResetLink($request->only('email'));
                $password_reset = DB::table('password_resets')->where('email', $request['email'])->first();
                if ($password_reset) {

                    $data = [
                        'title' => 'Forgot password',
                        'liens' =>config('frontend.routes.reset_password').'?token='.Crypt::encryptString($request->email),
                    ];
                    Mail::to(env('ADMIN_EMAIL'))->send(new ForgotPasswordMail($data));
                }
                return [
                    'succes' => "Send Email succefully",
                    'status_code' => 201
                ];
            }
        } catch (Exception $e) {
            return [
                'error' => 'something_went_wrong',
                'message' => $e->getMessage(),
                'status_code' => 500
            ];
        }
    }
}
