<?php

namespace App\Http\Controllers\Api;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    public function index()
    {
        $user = User::get();
        return response()->json($user);
    }
    public function create(Request $request)
    {
        try {
            $user = User::create([
                'nom' => $request['nom'],
                'prenom' => $request['prenom'],
                'email' => $request['email'],
                'tel' => $request['tel'],
                'adresse' => $request['adresse'],
                'code_postal' => $request['code_postal'],
                'ville' => $request['ville'],
                'date_naissance' => $request['date_naissance'],
                'is_admin' => false,
                'password' => Hash::make($request['password']),
                'username' => $request['username'],

            ]);
            return response()->json([
                'message' => 'created User succes !',
                'data'   => $user
            ]);
        } catch (\Throwable $th) {
            return [
                'message'     => $th->getMessage(),
                'status_code' => 501
            ];
        }
    }
    public function show($id)
    {
        $user = User::whereId($id)->first();
        return response()->json($user);
    }
    public function edit($id)
    {
        $user = User::find($id);
        return response()->json($user);
    }
    public function update(Request $request, $id)
    {
        try {
            $data = User::create([
                'nom' => $request['nom'],
                'prenom' => $request['prenom'],
                'email' => $request['email'],
                'tel' => $request['tel'],
                'adresse' => $request['adresse'],
                'code_postal' => $request['code_postal'],
                'ville' => $request['ville'],
                'date_naissance' => $request['date_naissance'],
                'is_admin' => false,
                'password' => Hash::make($request['password']),
                'username' => $request['username'],
            ]);

            $user = User::whereId($id)->update($data);
            return response()->json([
                'message' => 'updated user succes !',
                'data'   => $user
            ]);
        } catch (\Throwable $th) {
            return [
                'message'     => $th->getMessage(),
                'status_code' => 501
            ];
        }
    }
    public function delete($id)
    {
        User::whereId($id)->delete();
        return response()->json([
            'message' => 'Deleted user success!'
        ]);
    }

}
