<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TypeService;
class TypeServiceController extends Controller
{
    public function index()
    {
        $typeService = TypeService::select('id', 'prix','service')->get();
        return response()->json($typeService);
    }
    public function create(Request $request)
    {
        try {
            $typeService = TypeService::create([
                'prix'   => $request['prix'],
                'service' => $request['service']
            ]);
            return response()->json([
                'message' => 'created type service succes !',
                'data'   => $typeService
            ]);
        } catch (\Throwable $th) {
            return [
                'message'     => $th->getMessage(),
                'status_code' => 501
            ];
        }
    }
    public function edit($id)
    {
        $TypeService = TypeService::find($id);
        return response()->json($TypeService);
    }
    public function update(Request $request, $id)
    {
        try {
            $TypeService = TypeService::whereId($id)->update([
                'prix'    => $request['prix'],
                'service' => $request['service']
            ]);
            return response()->json([
                'message' => 'updated Type service succes !',
                'data'   => $TypeService
            ]);
        } catch (\Throwable $th) {
            return [
                'message'     => $th->getMessage(),
                'status_code' => 501
            ];
        }
    }
    public function delete($id)
    {
        TypeService::whereId($id)->delete();
        return response()->json([
            'message' => 'Deleted type service success!'
        ]);
    }
}
