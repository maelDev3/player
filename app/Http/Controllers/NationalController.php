<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect,Response;
use App\Models\{Player,ClubTeam,NationalTeam};

class NationalController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function __invoke()
    {
        $nationalteams = NationalTeam::select(['id','name','description'])->get();
        return view('national.index',compact('nationalteams')); 
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'   => 'required',
        ]);
        $nationalteam = NationalTeam::create(['name'=>$request->name,'description'=>$request->description]);
        return Response::json($nationalteam);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $nationalteam = NationalTeam::find($id);
        return Response::json($nationalteam);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $nationalteam = NationalTeam::find($id);
        if (!$nationalteam) {
            return response()->json(['error' => 'National not found'], 404);
        }
        $nationalteam->name = $request->name;
        $nationalteam->description = $request->description;
        $nationalteam->save();
        return response()->json($nationalteam);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $nationalteam = NationalTeam::where('id',$id)->delete();
        return Response::json($nationalteam);
    }
}
