<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect,Response;
use App\Models\{Player,ClubTeam,NationalTeam};

class ClubController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function __invoke()
    {
        $clubs = ClubTeam::select(['id','name','description'])->get();
        return view('club.index',compact('clubs')); 
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'   => 'required',
        ]);
        $club = ClubTeam::create(['name'=>$request->name,'description'=>$request->description]);
        return Response::json($club);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $club = ClubTeam::find($id);
        return Response::json($club);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $club = ClubTeam::find($id);
        if (!$club) {
            return response()->json(['error' => 'Club not found'], 404);
        }
        $club->name = $request->name;
        $club->description = $request->description;
        $club->save();
        return response()->json($club);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $club = ClubTeam::where('id',$id)->delete();
        return Response::json($club);
    }
}
