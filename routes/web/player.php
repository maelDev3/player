<?php

use App\Http\Controllers\PlayersController;
use Illuminate\Support\Facades\Route;

Route::prefix('players')->controller(PlayersController::class)->group(function() {
    Route::get('', '__invoke')->name('players.__invoke');
    Route::get('filter', 'filter')->name('players.filter');
    Route::get('create', 'create')->name('players.create');
    Route::post('', 'store')->name('players.store');
    Route::get('edit/{id}', 'edit')->name('players.edit');
    Route::put('update/{id}', 'update')->name('players.update');
    Route::delete('{id}', 'destroy')->name('players.destroy');
});
