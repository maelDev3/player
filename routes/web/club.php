<?php

use App\Http\Controllers\ClubController;
use Illuminate\Support\Facades\Route;

Route::prefix('club')->controller(ClubController::class)->group(function() {
    Route::get('', '__invoke')->name('club.__invoke');
    Route::post('', 'store')->name('club.store');
    Route::get('edit/{id}', 'edit')->name('club.edit');
    Route::put('update/{id}', 'update')->name('club.update');
    Route::delete('{id}', 'destroy')->name('club.destroy');
});
