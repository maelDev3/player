<?php

use App\Http\Controllers\NationalController;
use Illuminate\Support\Facades\Route;

Route::prefix('nationalteam')->controller(NationalController::class)->group(function() {
    Route::get('', '__invoke')->name('nationalteam.__invoke');
    Route::post('', 'store')->name('nationalteam.store');
    Route::get('edit/{id}', 'edit')->name('nationalteam.edit');
    Route::put('update/{id}', 'update')->name('nationalteam.update');
    Route::delete('{id}', 'destroy')->name('nationalteam.destroy');
});
