<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PlayersController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('layouts.master');    
// });

Route::prefix('/')->controller(PlayersController::class)->group(function() {
    Route::get('', '__invoke')->name('players.__invoke');
});

require_once 'web/club.php';
require_once 'web/nationalTeam.php';
require_once 'web/player.php';


