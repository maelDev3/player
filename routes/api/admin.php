<?php
use App\Http\Controllers\Api\AdminController;
use Illuminate\Support\Facades\Route;
Route::prefix('login')
    ->controller(AdminController::class)
    ->group(function () {
        Route::post('', 'authentificate');
    });

Route::prefix('forgot-password')->controller(AdminController::class)
    ->group(function () {
        Route::post('', 'forgotPasword');
});


