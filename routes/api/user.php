<?php
use App\Http\Controllers\Api\UserController;
use Illuminate\Support\Facades\Route;
Route::prefix('user')
    ->controller(UserController::class)
    // ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('', 'index');
        Route::get('/show/{id}', 'show');
        Route::get('/edit/{id}', 'edit');
        Route::post('', 'create');
        Route::put('/{id}', 'update');
        Route::delete('/{id}', 'delete');
    });
