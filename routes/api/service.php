<?php

use App\Http\Controllers\Api\ServiceController;
use Illuminate\Support\Facades\Route;
Route::prefix('service')
    ->controller(ServiceController::class)
    // ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('', 'index');
        Route::get('/edit/{id}', 'edit');
        Route::post('', 'create');
        Route::put('/{id}', 'update');
        Route::delete('/{id}', 'delete');
    });
