<?php
use App\Http\Controllers\Api\ReservationController;
use Illuminate\Support\Facades\Route;
Route::prefix('reservation')
    ->controller(ReservationController::class)
    // ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('', 'index');
        Route::get('/edit/{id}', 'edit');
        Route::post('', 'create');
        Route::put('/{id}', 'update');
        Route::delete('/{id}', 'delete');
        Route::get('/longlat', 'filterLongLat');
        
    });
