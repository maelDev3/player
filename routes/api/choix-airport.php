<?php

use App\Http\Controllers\Api\ChoixAeroportController;
use Illuminate\Support\Facades\Route;
Route::prefix('choicexairport')
    ->controller(ChoixAeroportController::class)
    ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('', 'index');
        Route::get('/edit/{id}', 'edit');
        Route::post('', 'create');
        Route::put('/{id}', 'update');
        Route::delete('/{id}', 'delete');
    });
