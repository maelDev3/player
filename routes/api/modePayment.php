<?php
use App\Http\Controllers\Api\ModePaymentController;
use Illuminate\Support\Facades\Route;
Route::prefix('modepayment')
    ->controller(ModePaymentController::class)
    ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('', 'index');
        Route::get('/edit/{id}', 'edit');
        Route::post('', 'create');
        Route::put('/{id}', 'update');
        Route::delete('/{id}', 'delete');
    });
