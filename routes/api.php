<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{CordinateController,AuthController};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/coordinates', [CordinateController::class, 'store']);
Route::get('/coordinates', [CordinateController::class, 'index']);
Route::post('/check-user', [AuthController::class, 'checkUserExists']);


Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'register']);
Route::middleware('auth:sanctum')->post('/logout', [AuthController::class, 'logout']);

require_once 'api/admin.php';
require_once 'api/service.php';
require_once 'api/vol.php';
require_once 'api/user.php';
require_once 'api/reservation.php';
require_once 'api/airports.php';
require_once 'api/typeservice.php';
// require_once 'api/pays.php';
// require_once 'api/modePayment.php';
// require_once 'api/memberClub.php';
// require_once 'api/voyageur.php';
// require_once 'api/partenaireService.php';
// require_once 'api/programmevol.php';
// require_once 'api/siege.php';
// require_once 'api/avion.php';
// require_once 'api/flight-share.php';
// require_once 'api/flight-fixe.php';
// require_once 'api/choix-siege.php';
// require_once 'api/choix-airport.php';
// require_once 'api/payment.php';

