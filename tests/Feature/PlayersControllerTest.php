<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Player;

class PlayersControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test store method.
     *
     * @return void
     */
    public function testStore()
    {
        // Créez des données de requête simulées pour tester le stockage d'un joueur
        $data = [
            'name' => 'Lionel',
            'goals' => 2,
            'club_id' => 1,
            'national_team_id' => 1
        ];

        // Effectuez une requête POST simulée à la route de stockage du joueur
        $response = $this->post('/players', $data);

        $this->assertDatabaseHas('players', ['name' => 'Messi']);
        
        $response->assertJsonFragment($data);
    }

    /**
     * Test update method.
     *
     * @return void
     */
    
    public function testUpdate()
    {
        // Créez un joueur simulé dans la base de données pour le mettre à jour ensuite
        $player = Player::factory()->create();
    
        // Données simulées pour la mise à jour du joueur
        $updateData = [
            'name' => 'Christiano',
            'goals' => 15,
            'club_id' => 2,
            'national_team_id' => 2
        ];
    
        // Effectuez une requête PUT simulée pour mettre à jour le joueur
        $response = $this->put("/players/{$player->id}", $updateData);
    
        // Assurez-vous que la réponse indique que la mise à jour a réussi
        $response->assertStatus(200); 
    
        // Vérifiez que le joueur a bien été mis à jour dans la base de données
        $this->assertDatabaseHas('players', ['id' => $player->id] + $updateData);
    }
    
    
    





    
}
