<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@stack('page_title') | Tableau de bord</title>
    
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.0.1/css/bootstrap.min.css">
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    

</head>

<body>
    <div class="app-admin-wrap layout-sidebar-large">
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">
              <img src="../../assets/images/boutique-marque.jpg" alt="Logo" height="30"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll"
                aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarScroll">
                <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('players.__invoke') }}">Joueurs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('club.__invoke') }}">Clubs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('nationalteam.__invoke') }}">Équipes nationales</a>
                    </li>
                </ul>
                <form class="d-flex">
                    <div class="user col align-self-end">
                        
                    </div>
                </form>
            </div>
        </div>
    </nav>
    


        <main class="">
            @yield('content')
        </main>
        @stack('js')

</body>

</html>
