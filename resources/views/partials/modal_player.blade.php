<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="postCrudModal"></h4>
            </div>
            <div class="modal-body">
                <form id="postForm" name="postForm" class="form-horizontal">

                   <input type="hidden" id="player_id" name="player_id" value="">

                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="name" name="name" value="" required="">
                        </div>
                    </div>
         
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Score</label>
                        <div class="col-sm-12">
                            <input type="number" class="form-control" id="goals" name="goals" value="" required="">
                        </div>
                    </div>

                    <div class="form-group mb-3">
                        <label  class="col-sm-2 control-label">Club</label>
                        <div class="col-sm-12">
                        <select name="club_id" class="form-control" id="club_id">
                        @foreach($clubs as $club)
                        <option value="{{$club->id}}" id="" >{{$club->name}}</option>
                        @endforeach
                        </select>
                        </div>
                    </div>

                    <div class="form-group mb-3">
                        <label  class="col-sm-3 control-label">National Team</label>
                        <div class="col-sm-12">
                        <select name="national_team_id" class="form-control" id="national_team_id">
                        @foreach($nationalteams as $nationalteam)
                        <option value="{{ $nationalteam->id }}">{{$nationalteam->name}}</option>
                        @endforeach>
                        </select>
                        </div>
                    </div>

                    <div class="col-sm-offset-2 col-sm-10 mt-2">
                     <button type="submit" class="btn btn-primary" id="btn-save" value="create">Save
                     </button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>