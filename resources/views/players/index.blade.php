@extends('layouts.master')
@push('page_title')
    Liste Players
@endpush

@section('content')
    <div class="container mt-5">

        <div class="main-content">
            <div class="breadcrumb">
              <h1>Liste Players</h1>
          </div>
          <div class="separator-breadcrumb border-top"></div>
                  <div class="d-flex justify-content-end">
                    <div class="input-group mr-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="club_filter">Club</label>
                        </div>
                        <select name="club_filter" class="custom-select form-control" id="club_filter">
                            <option value="">Filter by Club</option>
                            @foreach($clubs as $club)
                            <option value="{{$club->id}}">{{$club->name}}</option>
                            @endforeach
                        </select>
                    </div>
                
                    <div class="input-group mr-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="national_team_filter">National Team</label>
                        </div>
                        <select name="national_team_filter" class="custom-select form-control" id="national_team_filter">
                            <option value="">Filter by National Team</option>
                            @foreach($nationalteams as $nationalteam)
                            <option value="{{ $nationalteam->id }}">{{ $nationalteam->name }}</option>
                            @endforeach
                        </select>
                    </div>
                
                    <a href="javascript:void(0)" class="btn btn-primary" id="create-new-post">
                        <svg xmlns="http://www.w3.org/2000/svg" width="25px" height="25px" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 30 30" fill="#FFFFFF">
                            <path d="M15,3C8.373,3,3,8.373,3,15c0,6.627,5.373,12,12,12s12-5.373,12-12C27,8.373,21.627,3,15,3z M21,16h-5v5 c0,0.553-0.448,1-1,1s-1-0.447-1-1v-5H9c-0.552,0-1-0.447-1-1s0.448-1,1-1h5V9c0-0.553,0.448-1,1-1s1,0.447,1,1v5h5 c0.552,0,1,0.447,1,1S21.552,16,21,16z" fill="#FFFFFF" />
                        </svg>
                    </a>
                </div>
                
  
          <div class="container-fluid mt-5 ">
              <div class="row">
                  <div class="col-md-12">
                      <div class="card text-left">
                          <div class="card-body">
                              <div class="table-responsive ">
                                  <table class="display table table-striped table-bordered" id="deafult_ordering_table" style="width:100%">
                                      <thead>
                                          <tr>
                                              <th>Id</th>
                                              <th>Nom</th>
                                              <th>Score</th>
                                              <th>Club</th>
                                              <th>NationalTeam</th>
                                              <th>Actions</th>
                                          </tr>
                                      </thead>
                                      <tbody id="posts-crud">
                                              @foreach($players as $player)
                                              <tr id="post_id_{{ $player->id }}">
                                                  <td>{{ $player->id  }}</td>
                                                  <td>{{ $player->name }}</td>
                                                  <td>{{ $player->goals }}</td>
                                                  <td>{{ $player->club->name }}</td>
                                                  <td>{{ $player->nationalTeam->name }}</td>
                                                  <td>
                                                  <a href="javascript:void(0)" id="edit-post" data-id="{{ $player->id }}" class="btn btn-info"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24" fill="#FFFFFF"><path d="M5 3C3.895 3 3 3.895 3 5L3 19C3 20.105 3.895 21 5 21L16.171875 21L14.171875 19L5 19L5 9L19 9L18.998047 14.171875L21 16.171875L21 5C21 3.895 20.105 3 19 3L5 3 z M 5 5L19 5L19 7L5 7L5 5 z M 7 11L7 13L9 13L9 11L7 11 z M 11 11L11 13L17 13L17 11L11 11 z M 7 15L7 17L9 17L9 15L7 15 z M 11 15L11 17L13 17L13 15L11 15 z M 15 15L15 17L20.146484 22.146484L22.146484 20.146484L17 15L15 15 z M 22.853516 20.853516L20.853516 22.853516L21.853516 23.853516C22.048516 24.048516 22.365547 24.048516 22.560547 23.853516L23.853516 22.560547C24.048516 22.364547 24.048516 22.048516 23.853516 21.853516L22.853516 20.853516 z" fill="#FFFFFF" /></svg></a>
                                                  <a href="javascript:void(0)" id="delete-post" data-id="{{ $player->id }}" class="btn btn-danger delete-post"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16"><path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" /><path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" /></svg></a></td>
                                              </tr>
                                              @endforeach
                                          </tbody>
                                      <tfoot>
                                          <tr>
                                              <th>Id</th>
                                              <th>Nom</th>
                                              <th>Score</th>
                                              <th>club</th>
                                              <th>NationalTeam</th>
                                              <th>Actions</th>
                                          </tr>
                                      </tfoot>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
    </div>
    @include('partials.modal_player')
@endsection


@push('js')
<script type="module">
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#create-new-post').click(function() {
            $('#btn-save').val("create-post");
            $('#postForm').trigger("reset");
            $('#postCrudModal').html("Ajouter Player");
            $('#ajax-crud-modal').modal('show');
        });

        //Edit
        $('body').on('click', '#edit-post', function() {
            var player_id = $(this).data('id');
            $.get('players/edit/' + player_id, function(data) {
                $('#postCrudModal').html("Modifier Player");
                $('#btn-save').val("edit-post");
                $('#ajax-crud-modal').modal('show');
                $('#player_id').val(data.id);
                $('#name').val(data.name);
                $('#goals').val(data.goals);
                $('#club_id').val(data.club.id);
                $('#national_team_id').val(data.national_team.id);
            })
        });


        //Delete
        $('body').on('click', '.delete-post', function() {
            var player_id = $(this).data("id");
            confirm("Are You sure want to delete !");
            $.ajax({
                type: "DELETE",
                url: "{{ route('players.destroy', ':id') }}".replace(':id', player_id),
                success: function(data) {
                    $("#post_id_" + player_id).remove();
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });



        //Creer et update
        $('#postForm').submit(function(e) {
            e.preventDefault();
            var formData = new FormData($('#postForm')[0]);
            var actionType = $('#btn-save').val();
            var url = actionType == "create-post" ? "{{ route('players.store') }}" :
                "{{ route('players.update', ':id') }}";
            url = url.replace(':id', $('#player_id').val());
            if (actionType === 'edit-post') {
                formData.append('_method', 'PUT');
            }
            console.log('url', url)
            console.log('actionType', actionType)

            $.ajax({
                type: "POST",
                url: url,
                data: formData,
                dataType: "json",
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    console.log(data);
                    var post = '<tr id="post_id_' + data.id + '"><td>' + data.id +
                        '</td><td>' + data.name + '</td><td>' + data.goals + '</td><td>' +
                        data.club.name + '</td><td>' + data.national_team.name + '</td>';
                    post += '<td><a href="javascript:void(0)" id="edit-post" data-id="' +
                        data.id +
                        '" class="btn btn-info mr-1"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24" fill="#FFFFFF"><path d="M5 3C3.895 3 3 3.895 3 5L3 19C3 20.105 3.895 21 5 21L16.171875 21L14.171875 19L5 19L5 9L19 9L18.998047 14.171875L21 16.171875L21 5C21 3.895 20.105 3 19 3L5 3 z M 5 5L19 5L19 7L5 7L5 5 z M 7 11L7 13L9 13L9 11L7 11 z M 11 11L11 13L17 13L17 11L11 11 z M 7 15L7 17L9 17L9 15L7 15 z M 11 15L11 17L13 17L13 15L11 15 z M 15 15L15 17L20.146484 22.146484L22.146484 20.146484L17 15L15 15 z M 22.853516 20.853516L20.853516 22.853516L21.853516 23.853516C22.048516 24.048516 22.365547 24.048516 22.560547 23.853516L23.853516 22.560547C24.048516 22.364547 24.048516 22.048516 23.853516 21.853516L22.853516 20.853516 z" fill="#FFFFFF" /></svg></a><a href="javascript:void(0)" id="delete-post" data-id="' +
                        data.id +
                        '" class="btn btn-danger delete-post"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16"><path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" /><path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" /></svg></a></td></tr>';


                    if (actionType == "create-post") {
                        $('#posts-crud').prepend(post);
                    } else {
                        $("#post_id_" + data.id).replaceWith(post);
                    }

                    $('#postForm').trigger("reset");
                    $('#ajax-crud-modal').modal('hide');
                    $('#btn-save').html('Save Changes');
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#btn-save').html('Save Changes');
                }
            });
        });

        $('#club_filter, #national_team_filter').change(function() {
            var clubId = $('#club_filter').val();
            var nationalTeamId = $('#national_team_filter').val();

            // Effectuez une requête AJAX pour filtrer les données en fonction des filtres sélectionnés
            $.ajax({
                type: "GET",
                url: "{{ route('players.filter') }}",
                data: {
                    club_id: clubId,
                    national_team_id: nationalTeamId
                },
                success: function(data) {
                    console.log(data);

                    $('#posts-crud').empty();

                    // Iterate over each player object in the data array
                    $.each(data, function(index, player) {
                        console.log('player', player);
                        // Construct the HTML for a table row for each player
                        var row = '<tr id="post_id_' + player.id + '">' +
                            '<td>' + player.id + '</td>' +
                            '<td>' + player.name + '</td>' +
                            '<td>' + player.goals + '</td>' +
                            '<td>' + player.club.name + '</td>' +
                            '<td>' + player.national_team.name + '</td>' +
                            '<td>' +
                            '<a href="javascript:void(0)" class="btn btn-info mr-1 edit-post" data-id="' +
                            player.id +
                            '"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24" fill="#FFFFFF"><path d="M5 3C3.895 3 3 3.895 3 5L3 19C3 20.105 3.895 21 5 21L16.171875 21L14.171875 19L5 19L5 9L19 9L18.998047 14.171875L21 16.171875L21 5C21 3.895 20.105 3 19 3L5 3 z M 5 5L19 5L19 7L5 7L5 5 z M 7 11L7 13L9 13L9 11L7 11 z M 11 11L11 13L17 13L17 11L11 11 z M 7 15L7 17L9 17L9 15L7 15 z M 11 15L11 17L13 17L13 15L11 15 z M 15 15L15 17L20.146484 22.146484L22.146484 20.146484L17 15L15 15 z M 22.853516 20.853516L20.853516 22.853516L21.853516 23.853516C22.048516 24.048516 22.365547 24.048516 22.560547 23.853516L23.853516 22.560547C24.048516 22.364547 24.048516 22.048516 23.853516 21.853516L22.853516 20.853516 z" fill="#FFFFFF" /></svg></a>' +
                            '<a href="javascript:void(0)" class="btn btn-danger delete-post" data-id="' +
                            player.id +
                            '"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16"><path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" /><path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" /></svg></a>' +
                            '</td>' +
                            '</tr>';
                        $('#posts-crud').append(row);
                    });
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });
    });
</script>
@endpush
