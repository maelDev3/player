$("#datatable").dataTable({
    "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/French.json"
    }
});
ClassicEditor
    .create(document.querySelector("#description"), {
        toolbar: ['heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote'],
        heading: {
            options: [{
                    model: 'paragraph',
                    title: 'Paragraph',
                    class: 'ck-heading_paragraph'
                },
                {
                    model: 'heading1',
                    view: 'h1',
                    title: 'Heading 1',
                    class: 'ck-heading_heading1'
                },
                {
                    model: 'heading2',
                    view: 'h2',
                    title: 'Heading 2',
                    class: 'ck-heading_heading2'
                }
            ]
        }
    })
    .catch(error => {
        console.log(error);
    });

ClassicEditor
    .create(document.querySelector("#description-variant"), {
        toolbar: ['heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote'],
        heading: {
            options: [{
                    model: 'paragraph',
                    title: 'Paragraph',
                    class: 'ck-heading_paragraph'
                },
                {
                    model: 'heading1',
                    view: 'h1',
                    title: 'Heading 1',
                    class: 'ck-heading_heading1'
                },
                {
                    model: 'heading2',
                    view: 'h2',
                    title: 'Heading 2',
                    class: 'ck-heading_heading2'
                }
            ]
        }
    })
    .catch(error => {
        console.log(error);
    });

// Call filepond

filepondAdd();


$('#onProduct').val(0);
$('#onFacet').val(0);
$('#isSearchable').val(0);
$('#isRequired').val(0);
$('#isUsedForRecommendation').val(0);
$('#isEditableOnlyByAdmin').val(0);

$(document).on('change', '#onProduct', function () {
    // if checked
    if ($(this).is(':checked')) {
        $('#onProduct').val(1);
        $('#onProduct_hidden').val(1);
    } else {
        $('#onProduct').val(0);
        $('#onProduct_hidden').val(0);
    }
});
$(document).on('change', '#onFacet', function () {
    // if checked
    if ($(this).is(':checked')) {
        $('#onFacet').val(1);
        $('#onFacet_hidden').val(1);
    } else {
        $('#onFacet').val(0);
        $('#onFacet_hidden').val(0);
    }
});

$(document).on('change', '#isSearchable', function () {
    // if checked
    if ($(this).is(':checked')) {
        $('#isSearchable').val(1);
        $('#isSearchable_hidden').val(1);
    } else {
        $('#isSearchable').val(0);
        $('#isSearchable_hidden').val(0);
    }
});

$(document).on('change', '#isRequired', function () {
    // if checked
    if ($(this).is(':checked')) {
        $('#isRequired').val(1);
        $('#isRequired_hidden').val(1);
    } else {
        $('#isRequired').val(0);
        $('#isRequired_hidden').val(0);
    }
});

$(document).on('change', '#isUsedForRecommendation', function () {
    // if checked
    if ($(this).is(':checked')) {
        $('#isUsedForRecommendation').val(1);
        $('#isUsedForRecommendation_hidden').val(1);
    } else {
        $('#isUsedForRecommendation').val(0);
        $('#isUsedForRecommendation_hidden').val(0);
    }
});

$(document).on('change', '#isEditableOnlyByAdmin', function () {
    // if checked
    if ($(this).is(':checked')) {
        $('#isEditableOnlyByAdmin').val(1);
        $('#isEditableOnlyByAdmin_hidden').val(1);
    } else {
        $('#isEditableOnlyByAdmin').val(0);
        $('#isEditableOnlyByAdmin_hidden').val(0);
    }
});

$(document).on('change', '#attribut_group_id', function () {
    var type = $(this).val();
    if (type != "") {
        // ajax get attribut group by type
        $.ajax({
            url: getAttribut,
            type: "GET",
            data: {
                type: type
            },
            success: function (data) {
                // checked onProduct, onFacet, isSearchable, isRequired, isUsedForRecommendation, isEditableOnlyByAdmin

                if (data.onProduct == 1) {
                    $('#onProduct').prop('checked', true);
                    $('#onProduct').val(1);
                    $("#onProduct_hidden").val(1);
                    // disable onProduct
                    $('#onProduct').attr('disabled', true);
                } else {
                    $('#onProduct').prop('checked', false);
                    $('#onProduct').val(0);
                    $("#onProduct_hidden").val(0);
                    // enable onProduct
                    $('#onProduct').attr('disabled', false);
                }

                if (data.onFacet == 1) {
                    $('#onFacet').prop('checked', true);
                    $('#onFacet').val(1);
                    $("#onFacet_hidden").val(1);
                    // disable onFacet
                    $('#onFacet').attr('disabled', true);
                } else {
                    $('#onFacet').prop('checked', false);
                    $('#onFacet').val(0);
                    $("#onFacet_hidden").val(0);
                    // enable onFacet
                    $('#onFacet').attr('disabled', false);
                }

                if (data.isSearchable == 1) {
                    $('#isSearchable').prop('checked', true);
                    $('#isSearchable').val(1);
                    $("#isSearchable_hidden").val(1);
                    // disable isSearchable
                    $('#isSearchable').attr('disabled', true);
                } else {
                    $('#isSearchable').prop('checked', false);
                    $('#isSearchable').val(0);
                    $("#isSearchable_hidden").val(0);
                    // enable isSearchable
                    $('#isSearchable').attr('disabled', false);
                }

                if (data.isRequired == 1) {
                    $('#isRequired').prop('checked', true);
                    $('#isRequired').val(1);
                    $("#isRequired_hidden").val(1);
                    // disable isRequired
                    $('#isRequired').attr('disabled', true);
                } else {
                    $('#isRequired').prop('checked', false);
                    $('#isRequired').val(0);
                    $("#isRequired_hidden").val(0);
                    // enable isRequired
                    $('#isRequired').attr('disabled', false);
                }

                if (data.isUsedForRecommendation == 1) {
                    $('#isUsedForRecommendation').prop('checked', true);
                    $('#isUsedForRecommendation').val(1);
                    $("#isUsedForRecommendation_hidden").val(1);
                    // disable isUsedForRecommendation
                    $('#isUsedForRecommendation').attr('disabled', true);
                } else {
                    $('#isUsedForRecommendation').prop('checked', false);
                    $('#isUsedForRecommendation').val(0);
                    $("#isUsedForRecommendation_hidden").val(0);
                    // enable isUsedForRecommendation
                    $('#isUsedForRecommendation').attr('disabled', false);
                }

                if (data.isEditableOnlyByAdmin == 1) {
                    $('#isEditableOnlyByAdmin').prop('checked', true);
                    $('#isEditableOnlyByAdmin').val(1);
                    $("#isEditableOnlyByAdmin_hidden").val(1);
                    // disable isEditableOnlyByAdmin
                    $('#isEditableOnlyByAdmin').attr('disabled', true);
                } else {
                    $('#isEditableOnlyByAdmin').prop('checked', false);
                    $('#isEditableOnlyByAdmin').val(0);
                    $("#isEditableOnlyByAdmin_hidden").val(0);
                    // enable isEditableOnlyByAdmin
                    $('#isEditableOnlyByAdmin').attr('disabled', false);
                }
            }
        });
    } else {
        // enable onProduct, onFacet, isSearchable, isRequired, isUsedForRecommendation, isEditableOnlyByAdmin
        $('#onProduct').attr('disabled', false);
        $('#onFacet').attr('disabled', false);
        $('#isSearchable').attr('disabled', false);
        $('#isRequired').attr('disabled', false);
        $('#isUsedForRecommendation').attr('disabled', false);
        $('#isEditableOnlyByAdmin').attr('disabled', false);

        // uncheck onProduct, onFacet, isSearchable, isRequired, isUsedForRecommendation, isEditableOnlyByAdmin

        $('#onProduct').prop('checked', false);
        $('#onProduct').val(0);
        $("#onProduct_hidden").val(0);

        $('#onFacet').prop('checked', false);
        $('#onFacet').val(0);
        $("#onFacet_hidden").val(0);

        $('#isSearchable').prop('checked', false);
        $('#isSearchable').val(0);
        $("#isSearchable_hidden").val(0);

        $('#isRequired').prop('checked', false);
        $('#isRequired').val(0);
        $("#isRequired_hidden").val(0);

        $('#isUsedForRecommendation').prop('checked', false);
        $('#isUsedForRecommendation').val(0);
        $("#isUsedForRecommendation_hidden").val(0);

        $('#isEditableOnlyByAdmin').prop('checked', false);
        $('#isEditableOnlyByAdmin').val(0);
        $("#isEditableOnlyByAdmin_hidden").val(0);

    }

});
var type_edit_attr = $('#type_edit_attr').val();
// if variable type_edit_attr is exist
if (type_edit_attr != undefined) {
    if(type_edit_attr != ""){
        $('#variant-icon-pill').show();
        $("#variant-icon-pill-click").show();
        $("#variant-icon-pill-click").click(function () {
            $('html, body').animate({
                scrollTop: $("#variant-icon-pill").offset().top
            }, 1000);
            $('.nav-pills a[href="#variants"]').tab('show')
            $('#variant-icon-pill').show();
        });
    }else if(type_edit_attr == 1 || type_edit_attr == ""){
        $('#variant-icon-pill').hide();
        $("#variant-icon-pill-click").hide();
    }
}else{
    $('#variant-icon-pill').hide();
    $("#variant-icon-pill-click").hide();
}
$(document).on('change', '#type_attribut_id', function () {
    var type = $(this).val();
    if (type == 1 || type == 6 || type == 7 || type == 8) {
        $('#variant-icon-pill').hide();
        $("#variant-icon-pill-click").hide();
        // scroll to bottom with animation smooth
        $('html, body').animate({
            scrollTop: $("#variant-icon-pill").offset().top
        }, 1000);
    } else {
        $("#variant-icon-pill-click").show();
        // if click on variant-icon-pill-click then scroll to bottom with animation smooth

        $("#variant-icon-pill-click").click(function () {
            $('html, body').animate({
                scrollTop: $("#variant-icon-pill").offset().top
            }, 1000);
            $('.nav-pills a[href="#variants"]').tab('show')
            $('#variant-icon-pill').show();
        });
    }
});

var id = 1;

$('.multi-field-wrapper').each(function () {
    var $wrapper = $('.multi-fields', this);
    $(".add-field", $(this)).click(function (e) {
       // $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
       // append new field to wrapper and change name of input variant[0][value] to variant[1][value]
         $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus().attr('name', function (i, name) {
            return name.replace(/\[(\d+)\]/, function (str, p1) {
                // increase p1 by 1 when click add button
                return '[' + (parseInt(p1) + id) + ']';
            });
        });

        var imageUpload = `
            <div class="row">
                <div class="col-12 col-md-10 form-group">
                    <label for="name_image">
                        <b>Image</b>
                        <svg xmlns="http://www.w3.org/2000/svg" style="cursor: pointer" data-animation="true" data-html="true" data-toggle="tooltip" data-placement="top" title="
                            Image variant
                        " width="16px" height="16px" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 48 48">
                            <path fill="#8f8f8f" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"/><path fill="#fff" d="M22 22h4v11h-4V22zM26.5 16.5c0 1.379-1.121 2.5-2.5 2.5s-2.5-1.121-2.5-2.5S22.621 14 24 14 26.5 15.121 26.5 16.5z"/>
                        </svg>
                    </label>
                    <input type="file"
                        class="filepond"
                        name="name_image[${id}]"
                        data-max-file-size="55MB"
                        data-allow-reorder="true"
                    />
                </div>
                <div class="col-12 col-md-2"></div>
            </div>
            <div class="separator-breadcrumb border-top mt-2"></div>
        `;

        $('.multi-field:last-child', $wrapper).append(imageUpload);

        id++;

        filepondAdd();
    });
    $('.multi-field .row .remove-field', $wrapper).click(function () {
        if ($('.multi-field', $wrapper).length > 1) {
            // remove wrapper child
            $(this).parent().parent().parent().parent().remove();
        }
    });
});

// checkall parent and child
$(document).on('click', '#checkall', function () {
    console.log($(this).is(':checked'));
    if ($(this).is(':checked')) {
        $('.checkall').prop('checked', true);
    }
    else {
        $('.checkall').prop('checked', false);
    }
});


// function open tabs

function openTab(tabName) {
    $('.nav-pills a[href="#' + tab + '"]').tab('show');
}

$(document).on('change', '.parent', function () {
    // treeview
     if ($(this).is(':checked')) {
         $('.child').prop('checked', true);
     } else {
         $('.child').prop('checked', false);
     }
 });

$(document).on('change', '.parent-child', function () {
    // treeview
    if ($(this).is(':checked')) {
        $('.parent-child-child').prop('checked', true);
    } else {
        $('.parent-child-child').prop('checked', false);
    }
});


function filepondAdd() {
    /** Filepond */

    FilePond.registerPlugin(

        // encodes the file as base64 data
        FilePondPluginFileEncode,

        // validates the size of the file
        FilePondPluginFileValidateSize,

        // corrects mobile image orientation
        FilePondPluginImageExifOrientation,

        // previews dropped images
        FilePondPluginImagePreview,

        FilePondPluginImageEdit
    );

    // Set default FilePond options
    FilePond.setOptions({
        server: {
            url: action,
            process: {
                url: '/admin/upload',
            },
            restore: {
                url: '/storage/images/',
            },
            /*  revert: {
                    url: '/admin/delete',
                }, */
            headers: {
                'X-CSRF-TOKEN': csrf
            }
        }
    });

    // get data-files in input filepond


    // Select the file input and use create() to turn it into a pond
    FilePond.create(document.querySelector('input[name="name_image[]"]'), {
        labelIdle: 'Glissez vos fichiers ici ou <span class="filepond--label-action"> Parcourir </span>',
        labelFileLoading: 'Chargement',
        labelFileLoadError: 'Erreur de chargement',
        labelFileProcessing: 'En cours de traitement',
        labelFileProcessingComplete: 'Traitement terminé',
        labelFileProcessingAborted: 'Traitement annulé',
        labelFileProcessingError: 'Erreur de traitement',
        labelFileProcessingRevertError: 'Erreur de rétablissement',
        labelFileRemoveError: 'Erreur de suppression',
        labelButtonRemoveItem: 'Supprimer',
        labelButtonAbortItemLoad: 'Annuler',
        labelButtonRetryItemLoad: 'Réessayer',
        labelButtonAbortItemProcessing: 'Annuler',
        labelButtonUndoItemProcessing: 'Annuler',
        labelButtonRetryItemProcessing: 'Réessayer',
        labelButtonProcessItem: 'Envoyer',
        labelMaxFileSizeExceeded: 'Le fichier est trop volumineux',
        imagePreviewHeight: 170,
        imageCropAspectRatio: '1:1',
        styleLoadIndicatorPosition: 'center',
        styleProgressIndicatorPosition: 'right top',
        styleButtonRemoveItemPosition: 'top left',
        styleButtonProcessItemPosition: 'right bottom',
        chunkUploads: true
    });
    FilePond.parse(document.body);
}
