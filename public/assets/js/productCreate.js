window._currentValue = []

/**
 * EveryInputsIsFilled: mila feno ny input rehetra
 * condition1: raha vao mitovy amin'ilay ambony ny remise dia efa tsy validé
 * condition2: tsy tokony hitovy ny quantité min sy ny quantité max ary ny max tokony sup am min
 * condition3:
 */
class PriceValidator {
    currentValue = []

    /**
     * conditions
     */
    EveryInputsIsFilled = false
    condition1 = false
    condition2 = false
    consition3 = false

    errors = {
        condition1: 'La remise doit être unique',
        condition2: 'La quantité max doit être supérieur à la quantité min',
        condition3: 'La quantité min doit être supérieur à la quantité max de la remise précédente'
    }

    constructor(element) {
        this.element = element
        this.getDOMCurrentPrices = this.getDOMCurrentPrices.bind(this)
        this.listenInputChanges = this.listenInputChanges.bind(this)
        this.verifyCondition1 = this.verifyCondition1.bind(this)
        this.listenCondition3 = this.listenCondition3.bind(this)

        this.listenInputChanges()

        if(element.previousElementSibling) {
            //hanaovantsika ecout input jiaby
            this.getDOMCurrentPrices(element.previousElementSibling)

            this.listenCondition3()
        }
        else {
            this.condition1 = true
        }


    }


    getDOMCurrentPrices(previous) {
        const discount_percent = previous.querySelector('#discount_percent').value
        const quantity_min = previous.querySelector('#quantity_min').value
        const quantity_max = previous.querySelector('#quantity_max').value

        _currentValue.push({discount_percent: discount_percent, quantity_min: quantity_min, quantity_max: quantity_max})
        this.currentValue = _currentValue

        this.sortCurrentValues()

        this.verifyCondition1()

    }

    listenInputChanges() {
        this.element.querySelectorAll('input').forEach(input => {
            input.addEventListener('keyup', (e) => {
                this.EveryInputsIsFilled = this.verifyEveryInputsIsFilled(e.target.parentElement.parentElement.parentElement)

                this.showAddBtn()
            })
        })
    }

    verifyEveryInputsIsFilled(row) {
        const inputHasValues = []
        row.querySelectorAll('input').forEach(input => {
            !(input.value.trim() !== '') ? inputHasValues.push(false) :
            inputHasValues.push(true)
        })

        this.verifyCondition2();

        return !inputHasValues.includes(false)
    }


    verifyCondition1() {
        this.element.querySelector('#discount_percent').addEventListener('keyup', (e) => {
            this.condition1 = !(_currentValue.some((el) => {
                return el.discount_percent === e.target.value
            }))

            this.showErrorMessage('condition1')
        })
    }

    verifyCondition2() {
        let qte_min = this.element.querySelector('#quantity_min').value
        let qte_max = this.element.querySelector('#quantity_max').value

        if(!(qte_min.trim() === '' || qte_max.trim() === ''))
        {
            this.condition2 = (parseInt(qte_min) < parseInt(qte_max))
        }

        this.showErrorMessage('condition2')
    }

    listenCondition3() {
        let closestObject;
        let discount_percent;
        this.element.querySelectorAll('input').forEach(input => {

            if(input.getAttribute('id') !== 'discount_percent') {
                input.setAttribute('readonly', true)
            }

            input.addEventListener('keyup', (e) => {
                if(this.condition1) {
                    if(input.getAttribute('id') === 'discount_percent') {
                        closestObject = this.closestNumber(input.value)
                        discount_percent = this.getRowDiscountPercent(this.element)
                        this.element.querySelectorAll('input').forEach(input => {
                            input.removeAttribute('readonly')
                        })
                    }

                    if(input.getAttribute('id') === 'quantity_min') {
                        // console.log('valeur hanaovana an ilay comparaison', closestObject)
                        // console.log('valeur ho comparena', input.value)

                        if(discount_percent > parseInt(closestObject.discount_percent)) {
                            this.condition3 = closestObject.quantity_max < parseInt(input.value)

                            this.showErrorMessage('condition3')
                        }
                    }
                }
            })
        })

    }

    showAddBtn() {
        if(this.EveryInputsIsFilled && this.condition1 && this.condition2) {
            this.element.parentElement.parentElement.querySelectorAll('.add-field').forEach(btn => {
                btn.removeAttribute('readonly')
            })
        }
        else {
            this.element.parentElement.parentElement.querySelectorAll('.add-field').forEach(btn => {
                btn.setAttribute('readonly', true)
            })
        }
    }

    sortCurrentValues() {
        _currentValue = _currentValue.sort((a, b) => {
            return a.discount_percent - b.discount_percent
        })
    }

    closestNumber(number) {
        return _currentValue.reduce(function(prev, curr) {
            return (Math.abs(curr.discount_percent - number) < Math.abs(prev.discount_percent - number) ? curr : prev);
        });
    }

    showErrorMessage(condition) {
        if(!this[condition]) {
            this.element.querySelectorAll(`.error-${condition}`).forEach(el => {
                el.innerHTML = this.errors[condition]
            })
        }
        else {
            this.element.querySelectorAll(`.error-${condition}`).forEach(el => {
                el.innerHTML = ''
            })
        }
    }

    getRowDiscountPercent(row) {
        return parseInt(row.querySelector('#discount_percent').value)
    }
    /**
     *
     * @returns {PriceValidator[HTMLElement]}
     */
    static getAllInput() {
        return Array.from(document.querySelectorAll('.multi-field')).map(element => {
            return new PriceValidator(element)
        })
    }
}

window._pv = PriceValidator;
PriceValidator.getAllInput()
