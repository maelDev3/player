$("#datatable").dataTable({
    "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/French.json"
    }
});
ClassicEditor
    .create(document.querySelector("#description"), {
        toolbar: ['heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote'],
        heading: {
            options: [{
                    model: 'paragraph',
                    title: 'Paragraph',
                    class: 'ck-heading_paragraph'
                },
                {
                    model: 'heading1',
                    view: 'h1',
                    title: 'Heading 1',
                    class: 'ck-heading_heading1'
                },
                {
                    model: 'heading2',
                    view: 'h2',
                    title: 'Heading 2',
                    class: 'ck-heading_heading2'
                }
            ]
        }
    })
    .catch(error => {
        console.log(error);
    });

ClassicEditor
    .create(document.querySelector("#description-variant"), {
        toolbar: ['heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote'],
        heading: {
            options: [{
                    model: 'paragraph',
                    title: 'Paragraph',
                    class: 'ck-heading_paragraph'
                },
                {
                    model: 'heading1',
                    view: 'h1',
                    title: 'Heading 1',
                    class: 'ck-heading_heading1'
                },
                {
                    model: 'heading2',
                    view: 'h2',
                    title: 'Heading 2',
                    class: 'ck-heading_heading2'
                }
            ]
        }
    })
    .catch(error => {
        console.log(error);
    });

// Call filepond

filepondAdd();


$('#onProduct').val(0);
$('#onFacet').val(0);
$('#isSearchable').val(0);
$('#isRequired').val(0);
$('#isUsedForRecommendation').val(0);
$('#isEditableOnlyByAdmin').val(0);

$(document).on('change', '#onProduct', function () {
    // if checked
    if ($(this).is(':checked')) {
        $('#onProduct').val(1);
        $('#onProduct_hidden').val(1);
    } else {
        $('#onProduct').val(0);
        $('#onProduct_hidden').val(0);
    }
});
$(document).on('change', '#onFacet', function () {
    // if checked
    if ($(this).is(':checked')) {
        $('#onFacet').val(1);
        $('#onFacet_hidden').val(1);
    } else {
        $('#onFacet').val(0);
        $('#onFacet_hidden').val(0);
    }
});

$(document).on('change', '#isSearchable', function () {
    // if checked
    if ($(this).is(':checked')) {
        $('#isSearchable').val(1);
        $('#isSearchable_hidden').val(1);
    } else {
        $('#isSearchable').val(0);
        $('#isSearchable_hidden').val(0);
    }
});

$(document).on('change', '#isRequired', function () {
    // if checked
    if ($(this).is(':checked')) {
        $('#isRequired').val(1);
        $('#isRequired_hidden').val(1);
    } else {
        $('#isRequired').val(0);
        $('#isRequired_hidden').val(0);
    }
});

$(document).on('change', '#isUsedForRecommendation', function () {
    // if checked
    if ($(this).is(':checked')) {
        $('#isUsedForRecommendation').val(1);
        $('#isUsedForRecommendation_hidden').val(1);
    } else {
        $('#isUsedForRecommendation').val(0);
        $('#isUsedForRecommendation_hidden').val(0);
    }
});

$(document).on('change', '#isEditableOnlyByAdmin', function () {
    // if checked
    if ($(this).is(':checked')) {
        $('#isEditableOnlyByAdmin').val(1);
        $('#isEditableOnlyByAdmin_hidden').val(1);
    } else {
        $('#isEditableOnlyByAdmin').val(0);
        $('#isEditableOnlyByAdmin_hidden').val(0);
    }
});

$(document).on('change', '#attribut_group_id', function () {
    var type = $(this).val();
    if (type != "") {
        // ajax get attribut group by type
        $.ajax({
            url: getAttribut,
            type: "GET",
            data: {
                type: type
            },
            success: function (data) {
                // checked onProduct, onFacet, isSearchable, isRequired, isUsedForRecommendation, isEditableOnlyByAdmin

                if (data.onProduct == 1) {
                    $('#onProduct').prop('checked', true);
                    $('#onProduct').val(1);
                    $("#onProduct_hidden").val(1);
                    // disable onProduct
                    $('#onProduct').attr('disabled', true);
                } else {
                    $('#onProduct').prop('checked', false);
                    $('#onProduct').val(0);
                    $("#onProduct_hidden").val(0);
                    // enable onProduct
                    $('#onProduct').attr('disabled', false);
                }

                if (data.onFacet == 1) {
                    $('#onFacet').prop('checked', true);
                    $('#onFacet').val(1);
                    $("#onFacet_hidden").val(1);
                    // disable onFacet
                    $('#onFacet').attr('disabled', true);
                } else {
                    $('#onFacet').prop('checked', false);
                    $('#onFacet').val(0);
                    $("#onFacet_hidden").val(0);
                    // enable onFacet
                    $('#onFacet').attr('disabled', false);
                }

                if (data.isSearchable == 1) {
                    $('#isSearchable').prop('checked', true);
                    $('#isSearchable').val(1);
                    $("#isSearchable_hidden").val(1);
                    // disable isSearchable
                    $('#isSearchable').attr('disabled', true);
                } else {
                    $('#isSearchable').prop('checked', false);
                    $('#isSearchable').val(0);
                    $("#isSearchable_hidden").val(0);
                    // enable isSearchable
                    $('#isSearchable').attr('disabled', false);
                }

                if (data.isRequired == 1) {
                    $('#isRequired').prop('checked', true);
                    $('#isRequired').val(1);
                    $("#isRequired_hidden").val(1);
                    // disable isRequired
                    $('#isRequired').attr('disabled', true);
                } else {
                    $('#isRequired').prop('checked', false);
                    $('#isRequired').val(0);
                    $("#isRequired_hidden").val(0);
                    // enable isRequired
                    $('#isRequired').attr('disabled', false);
                }

                if (data.isUsedForRecommendation == 1) {
                    $('#isUsedForRecommendation').prop('checked', true);
                    $('#isUsedForRecommendation').val(1);
                    $("#isUsedForRecommendation_hidden").val(1);
                    // disable isUsedForRecommendation
                    $('#isUsedForRecommendation').attr('disabled', true);
                } else {
                    $('#isUsedForRecommendation').prop('checked', false);
                    $('#isUsedForRecommendation').val(0);
                    $("#isUsedForRecommendation_hidden").val(0);
                    // enable isUsedForRecommendation
                    $('#isUsedForRecommendation').attr('disabled', false);
                }

                if (data.isEditableOnlyByAdmin == 1) {
                    $('#isEditableOnlyByAdmin').prop('checked', true);
                    $('#isEditableOnlyByAdmin').val(1);
                    $("#isEditableOnlyByAdmin_hidden").val(1);
                    // disable isEditableOnlyByAdmin
                    $('#isEditableOnlyByAdmin').attr('disabled', true);
                } else {
                    $('#isEditableOnlyByAdmin').prop('checked', false);
                    $('#isEditableOnlyByAdmin').val(0);
                    $("#isEditableOnlyByAdmin_hidden").val(0);
                    // enable isEditableOnlyByAdmin
                    $('#isEditableOnlyByAdmin').attr('disabled', false);
                }
            }
        });
    } else {
        // enable onProduct, onFacet, isSearchable, isRequired, isUsedForRecommendation, isEditableOnlyByAdmin
        $('#onProduct').attr('disabled', false);
        $('#onFacet').attr('disabled', false);
        $('#isSearchable').attr('disabled', false);
        $('#isRequired').attr('disabled', false);
        $('#isUsedForRecommendation').attr('disabled', false);
        $('#isEditableOnlyByAdmin').attr('disabled', false);

        // uncheck onProduct, onFacet, isSearchable, isRequired, isUsedForRecommendation, isEditableOnlyByAdmin

        $('#onProduct').prop('checked', false);
        $('#onProduct').val(0);
        $("#onProduct_hidden").val(0);

        $('#onFacet').prop('checked', false);
        $('#onFacet').val(0);
        $("#onFacet_hidden").val(0);

        $('#isSearchable').prop('checked', false);
        $('#isSearchable').val(0);
        $("#isSearchable_hidden").val(0);

        $('#isRequired').prop('checked', false);
        $('#isRequired').val(0);
        $("#isRequired_hidden").val(0);

        $('#isUsedForRecommendation').prop('checked', false);
        $('#isUsedForRecommendation').val(0);
        $("#isUsedForRecommendation_hidden").val(0);

        $('#isEditableOnlyByAdmin').prop('checked', false);
        $('#isEditableOnlyByAdmin').val(0);
        $("#isEditableOnlyByAdmin_hidden").val(0);

    }

});
var type_edit_attr = $('#type_edit_attr').val();
// if variable type_edit_attr is exist
if (type_edit_attr != undefined) {
    if(type_edit_attr != ""){
        $('#variant-icon-pill').show();
        $("#variant-icon-pill-click").show();
        $("#variant-icon-pill-click").click(function () {
            $('html, body').animate({
                scrollTop: $("#variant-icon-pill").offset().top
            }, 1000);
            $('.nav-pills a[href="#variants"]').tab('show')
            $('#variant-icon-pill').show();
        });
    }else if(type_edit_attr == 1 || type_edit_attr == ""){
        $('#variant-icon-pill').hide();
        $("#variant-icon-pill-click").hide();
    }
}else{
    $('#variant-icon-pill').hide();
    $("#variant-icon-pill-click").hide();
}
$(document).on('change', '#type_attribut_id', function () {
    var type = $(this).val();
    if (type == 1 || type == 6 || type == 7 || type == 8) {
        $('#variant-icon-pill').hide();
        $("#variant-icon-pill-click").hide();
        // scroll to bottom with animation smooth
        $('html, body').animate({
            scrollTop: $("#variant-icon-pill").offset().top
        }, 1000);
    } else {
        $("#variant-icon-pill-click").show();
        // if click on variant-icon-pill-click then scroll to bottom with animation smooth

        $("#variant-icon-pill-click").click(function () {
            $('html, body').animate({
                scrollTop: $("#variant-icon-pill").offset().top
            }, 1000);
            $('.nav-pills a[href="#variants"]').tab('show')
            $('#variant-icon-pill').show();
        });
    }
});

// checkall parent and child
$(document).on('click', '#checkall', function () {
    console.log($(this).is(':checked'));
    if ($(this).is(':checked')) {
        // check all
        $('.parent').prop('checked', true);
        // check all child for parent
        $('.parent').each(function () {
            var id = $(this).val();
            $('.child-' + id).prop('checked', true);
        });
    } else {
        $('.parent').prop('checked', false);
        // uncheck all child for parent
        $('.parent').each(function () {
            var id = $(this).val();
            $('.child-' + id).prop('checked', false);
        });
    }
});

checkchildren(true, false);

// function open tabs

function openTab(tabName) {
    $('.nav-pills a[href="#' + tab + '"]').tab('show');
}

// if checked parent then checked all child
function checkchildren(checked = true, unchecked = false) {

    $(document).on('change', '.parent', function () {
        var parent = $(this);
        var parent_id = $(this).val();
        if (parent.is(':checked')) {
            $('.child-' + parent_id).prop('checked', checked);
        } else {
            $('.child-' + parent_id).prop('checked', unchecked);
        }
    });
}


function filepondAdd() {
    /** Filepond */

    FilePond.registerPlugin(

        // encodes the file as base64 data
        FilePondPluginFileEncode,

        // validates the size of the file
        FilePondPluginFileValidateSize,

        // corrects mobile image orientation
        FilePondPluginImageExifOrientation,

        // previews dropped images
        FilePondPluginImagePreview,

        FilePondPluginImageEdit
    );

    // Set default FilePond options
    FilePond.setOptions({
        server: {
            url: action,
            process: {
                url: '/admin/upload',
            },
            restore: {
                url: '/storage/images/',
            },
            /*  revert: {
                    url: '/admin/delete',
                }, */
            headers: {
                'X-CSRF-TOKEN': csrf
            }
        }
    });

    // get data-files in input filepond


    // Select the file input and use create() to turn it into a pond
    FilePond.create(document.querySelector('input[name="name_image[]"]'), {
        labelIdle: 'Glissez vos fichiers ici ou <span class="filepond--label-action"> Parcourir </span>',
        labelFileLoading: 'Chargement',
        labelFileLoadError: 'Erreur de chargement',
        labelFileProcessing: 'En cours de traitement',
        labelFileProcessingComplete: 'Traitement terminé',
        labelFileProcessingAborted: 'Traitement annulé',
        labelFileProcessingError: 'Erreur de traitement',
        labelFileProcessingRevertError: 'Erreur de rétablissement',
        labelFileRemoveError: 'Erreur de suppression',
        labelButtonRemoveItem: 'Supprimer',
        labelButtonAbortItemLoad: 'Annuler',
        labelButtonRetryItemLoad: 'Réessayer',
        labelButtonAbortItemProcessing: 'Annuler',
        labelButtonUndoItemProcessing: 'Annuler',
        labelButtonRetryItemProcessing: 'Réessayer',
        labelButtonProcessItem: 'Envoyer',
        labelMaxFileSizeExceeded: 'Le fichier est trop volumineux',
        imagePreviewHeight: 170,
        imageCropAspectRatio: '1:1',
        styleLoadIndicatorPosition: 'center',
        styleProgressIndicatorPosition: 'right top',
        styleButtonRemoveItemPosition: 'top left',
        styleButtonProcessItemPosition: 'right bottom',
        chunkUploads: true
    });
    FilePond.parse(document.body);
}
