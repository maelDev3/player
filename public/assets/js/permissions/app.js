class PermissionClass {

    constructor(element) {
        this.element = element;
        this.whenChange = this.whenChange.bind(this)
        this.togglePermission = this.togglePermission.bind(this)

        this.element.addEventListener('change', this.whenChange)
    }

    async whenChange() {
        await this.togglePermission();
    }

    /**
     * @returns {Promise<void>}
     */
    async togglePermission() {
        const response = await fetch('/admin/permissions/toggle', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
            },
            body: JSON.stringify({
                role_id: parseInt(this.element.getAttribute('role-id')),
                permission_id: parseInt(this.element.getAttribute('permission-id'))
            })
        })

        response.json().then(res => {
            toastr.success(res.message)
        })
    }

    /**
     * @returns {PermissionClass[HTMLElement]}
     */
    static listen() {
        return Array.from(document.querySelectorAll('.permission_toggler')).map(function(element) {
            return new PermissionClass(element)
        })
    }
}

PermissionClass.listen()
