const url_region = "https://restcountries.com/v3.1/region/"
const url_state = "https://www.universal-tutorial.com/api/states/"
let auth_token;
const form_countries = document.getElementById('form_group_countries')
const form_states = document.getElementById('form_group_states')
const select_countries = document.getElementById("select_countries")
const select_states = document.getElementById('select_states')
const btn_select_region = document.getElementById('btn_select_region_container')
const all_region_selected = document.getElementById('all_region_selected')
const add_list_method_shipping = document.querySelector('#add-list-method-shipping')


var regions = []
var region_tmp = {
    continent: "",
    country: "",
    state: ""
}

function selectContinent(event) {
    var continent_selected = event.target.value
    const countries_view = document.getElementById("select_countries")

    const promise_continent = fetch(`${url_region}${continent_selected}`)


    promise_continent.then(response => {
        return response.json()
    }).then(all_country => {

        form_countries.style.display = "block"
        select_countries.innerHTML = ''
        if (all_country.length > 0) {

            all_country.forEach(country => {
                let select_country = document.createElement('option')
                select_country.setAttribute("value", country.name.common)
                select_country.textContent = country.name.common
                select_countries.appendChild(select_country)
            })
            // countries = countries[0].countries
            region_tmp.continent = event.target.textContent

        }

    })


}

function selectStateByCountry(event) {
    var country_code = event.target.value
    const state_view = document.getElementById('select_states')

    console.log(auth_token)
    let promise_state = fetch(`${url_state}${country_code}`, {
        method: "get",
        headers: {
            'Authorization': `Bearer ${auth_token}`,
            'Accept': "application/json"
        }
    })

    promise_state.then(response => {
        return response.json()
    }).then(all_state => {

        form_states.style.display = "block"
        select_states.innerHTML = ""
        all_state.forEach(state => {
            option_state = document.createElement("option")
            option_state.setAttribute("value", state.state_name)
            option_state.textContent = state.state_name
            select_states.appendChild(option_state)
        })
        region_tmp.country = country_code

        btn_select_region.style.display = "block"
    })



}


function selectState(event) {
    region_tmp.state = event.target.value
}

function addRegion(event) {
    event.preventDefault()
    region_tmp.id = regions.length + 1
    regions.push(region_tmp)

    const

        region_view = `
    <div id="region-${region_tmp.id}" class="btn-group mr-2 mb-2" role="group">
     <button class="btn">${region_tmp.continent} <i
     data-continent="${region_tmp.continent}"
     data-countrie="${region_tmp.country}"
     data-state="${region_tmp.state}"
     data-region-id="${region_tmp.id}"
     onclick="removeRegion(event)"
     class="i-Close"></i></button>
    </div>
    `
    all_region_selected.innerHTML += region_view

    initAllSelect()
    console.log(regions);
}


function removeRegion(event) {
    event.preventDefault()
    const current_id = event.target.dataset.regionId
    const parent_node_region = document.querySelector("#region-" + current_id + "")

    parent_node_region.parentNode.removeChild(parent_node_region)

    console.log(parent_node_region.parentNode)

    updateData(regions, current_id)


    console.log(regions)

}


function updateData(target, index) {
    indexObject = target.findIndex(object => {
        return object.id === index
    })

    target.splice(indexObject, 1)
}

function initAllSelect() {
    region_tmp = {}
    form_countries.style.display = "none";
    form_states.style.display = "none";
    select_states.innerHTML = "";
    select_countries.innerHTML = "";
    btn_select_region.style.display = "none";
}

$(document).ready(function () {

    $.ajax({
        type: "get",
        url: "https://www.universal-tutorial.com/api/getaccesstoken",
        headers: {
            'api-token': 'oojMqU0vPbWS9Fl3X5Pr6uRo_z9gBXiWrkKIrFve9q5bufjvfaU0ulBcUM_jAMhOoGg',
            'user-email': 'rastaitrbeatz320@gmail.com',
            'Accept': "application/json"
        },
        success: function (data) {
            auth_token = data.auth_token
        }
    });
})



document.getElementById('form_zone')
    .addEventListener('submit', (e) => {
        e.preventDefault()
        const form = new FormData(e.target)

        const zone_name = form.get('zone_name')
        console.log(form.get('status'))

        
        var data = {
            "continent": form.get('continent'),
            "country": form.get('country'),
            "state": form.get('state'),
            "zone_name": form.get("zone_name"),
            "id": parseInt(form.get("id")),
            "price": form.get('price'),
            "status": form.get('status')



        }
        var json = JSON.stringify(data);

        fetch(urlApiShipping, {
            method: 'POST',
            headers: { 'Content-type': 'application/json '},
            body: json
        })
            .then(raw => raw.json())
            .then(data => {
                if(!data.error) {
                    window.location = previous_url
                }
            })
            .catch((err) => console.log(err));

    })


