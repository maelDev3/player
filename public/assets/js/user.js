new_avatar_user = document.querySelector('#user-avatar-updated')


$("#user-list").dataTable({
  "order": [
    [0, "desc"]
  ],
  "language": {
    "lengthMenu": "Afficher _MENU_",
    "zeroRecords": "Aucun enregistrement trouvé",
    "info": "Affichage de la page _PAGE_ sur _PAGES_",
    "infoEmpty": "Aucun enregistrement disponible",
    "infoFiltered": "(filtré à partir de _MAX_ enregistrements)",
    "search": "Rechercher",
    "paginate": {
      "previous": "Précédent",
      "next": "Suivant"
    }
  },
  lengthMenu: [
    [10, 25, 50, -1],
    ['10', '25', '50', 'Tout']
  ]
});


function openModal(event) {
  const dataset = event.target.dataset 
  const data = document.querySelector('#avatar-file')
  const userFullName = document.querySelector('#user-full-name')
  const companyName = document.querySelector('#company')
  const email = document.querySelector('#email')
  const phone = document.querySelector('#phone')
  const companyAddress = document.querySelector("#address")
  const store = document.querySelector('#store')


  data.setAttribute('data-user-id', dataset.idUser)
  userFullName.textContent = dataset.user //image
  new_avatar_user.src = dataset.image
  companyName.textContent = dataset.company
  email.textContent = dataset.email 
  phone.textContent = dataset.phone
  companyAddress.textContent = dataset.address
  store.textContent = dataset.store

  console.log(companyAddress)
  console.log(event.target.dataset)
}



