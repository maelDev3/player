var i = 1;
function formulaire(el=null,index=null)
{
    return `<div>
            <div class="row">
                        <div class="col-md-4">
                            <label for="name"><b>Nom *</b></label>
                            <input required="required" class="form-control input_enfant" id="value" name="variant_value[${i}][value]" type="text" placeholder="L,XL,XXL,.." />
                        </div>
                        
                        <div class="col-md-4">
                            <label for="name"><b>Quantité</b></label>
                            <input  class="form-control input_enfant" id="amount" name="variant_value[${i}][amount]" type="number" min="0" placeholder="Quantite" />
                        </div>

                        <div class="col-md-4">
                                <label for="Capacity"><b>Capacite </b></label>
                            <input  class="form-control input_enfant" id="capacity" name="variant_value[${i}][capacity]" type="text" placeholder="Capacite"/>
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 mt-3">
                                <label for="price">
                                    <b>PGHT</b>
                                    <svg xmlns="http://www.w3.org/2000/svg" style="cursor: pointer" data-animation="true" data-html="true" data-toggle="tooltip" data-placement="top" title="
                                    Prix de gros HT du produit
                                    " width="16px" height="16px" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 48 48">
                                        <path fill="#8f8f8f" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"/><path fill="#fff" d="M22 22h4v11h-4V22zM26.5 16.5c0 1.379-1.121 2.5-2.5 2.5s-2.5-1.121-2.5-2.5S22.621 14 24 14 26.5 15.121 26.5 16.5z"/>
                                    </svg>
                                </label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend"><span class="input-group-text" >€</span></div>
                                    <input class="form-control" name="variant_value[${i}][price_pght]" value="" id="price_pght" type="text" placeholder="Prix" aria-label="price" aria-describedby="price" required />
                                </div>     
                            </div>
                            
                            <div class="col-md-4 mt-3">
                             <label for="price">
                                    <b>PPI</b>
                                    <svg xmlns="http://www.w3.org/2000/svg" style="cursor: pointer" data-animation="true" data-html="true" data-toggle="tooltip" data-placement="top" title="
                                    Prix public indicatif unitaire
                                    " width="16px" height="16px" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 48 48">
                                        <path fill="#8f8f8f" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"/><path fill="#fff" d="M22 22h4v11h-4V22zM26.5 16.5c0 1.379-1.121 2.5-2.5 2.5s-2.5-1.121-2.5-2.5S22.621 14 24 14 26.5 15.121 26.5 16.5z"/>
                                    </svg>
                                </label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend"><span class="input-group-text" >€</span></div>
                                    <input class="form-control" name="variant_value[${i}][price_ppi]" value="" id="price_ppi" type="text" placeholder="Prix" aria-label="price_ppi" aria-describedby="price_ppi" required />
                                </div>        
                            </div>

                            <div class="col-md-4 mt-3">
                                <label for="name"><b>Code</b></label>
                                <div class="input-group mb-3">
                                    <input class="form-control" name="variant_value[${i}][coded]" value="" id="coded" type="text" maxlength="13" placeholder="code" aria-label="coded" aria-describedby="coded" required />
                                </div> 
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-4 mt-3">
                              <label for="name"><b>Largeur</b>
                              
                                    <svg xmlns="http://www.w3.org/2000/svg" style="cursor: pointer" data-animation="true" data-html="true" data-toggle="tooltip" data-placement="top" title="
                                    Largeur
                                    " width="16px" height="16px" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 48 48">
                                        <path fill="#8f8f8f" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"/><path fill="#fff" d="M22 22h4v11h-4V22zM26.5 16.5c0 1.379-1.121 2.5-2.5 2.5s-2.5-1.121-2.5-2.5S22.621 14 24 14 26.5 15.121 26.5 16.5z"/>
                                    </svg>
                              </label>
                                <input  class="form-control input_enfant" id="weight" name="variant_value[${i}][weight]" type="number" min="0" placeholder="Largeur" />
                            </div>
                            <div class="col-md-4 mt-3">
                                <label for="name"><b>Dimension</b>
                                    <svg xmlns="http://www.w3.org/2000/svg" style="cursor: pointer" data-animation="true" data-html="true" data-toggle="tooltip" data-placement="top" title="
                                  Dimension
                                    " width="16px" height="16px" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 48 48">
                                        <path fill="#8f8f8f" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"/><path fill="#fff" d="M22 22h4v11h-4V22zM26.5 16.5c0 1.379-1.121 2.5-2.5 2.5s-2.5-1.121-2.5-2.5S22.621 14 24 14 26.5 15.121 26.5 16.5z"/>
                                    </svg>
                                </label>
                                <input  class="form-control input_enfant" id="dimension" name="variant_value[${i}][dimension]" type="number" min="0" placeholder="dimension" />
                            </div>
                            <div class="col-md-4 mt-3">
                                <label for="images-${i+1}"> Upload Image</label>
                                <input type="file" id="images-${i+1}"  name="variant_value[${i}][file][]" accept="image/*"  style="visibility: hidden; position: absolute;" multiple/>    
                            </div>
                          </div>
                        <div class="row">
                            <div class="col-md-12">
                             <div id="multiple-file-preview">
                                <div id="sortable-${i+1}" class="list_img mt-3 d-flex">
                                    <div class="clear-both">
                                    </div>
                                </div>
                             </div>   
                            </div>
                        </div>
                        <div class="col-md-12 mt-3 mb-2">
                            <div class="d-flex justify-content-end">
                                <button class="btn btn-primary ml-2 addboutton" onclick="return ADDINPUT(this)">
                                    +
                                </button>
                                <button class="btn btn-danger ml-2" onclick="return DELETEINPUT(this)" id="form_delete">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                        <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                    </svg>
                                </button>
                            </div>
                        </div>    
</div> 
`
}
var INPUTNAME = [];
function showinputadd() {
    INPUTNAME.forEach((element,index) => {
        $('#add_input_name').append(element, index);
        // formulaire(element, index)
    })
}
showinputadd(); 

function ADDINPUT() {
    document.querySelector(".addboutton").addEventListener("click", function (event) {
        event.preventDefault();
    }, false)
   
    INPUTNAME.push(formulaire(null, i));
    showinputadd();
    INPUTNAME.shift(formulaire(null, i));
    
    i++;
    insert_image();
    return false;
}


function DELETEINPUT(obj) {
    obj.parentElement.parentElement.parentElement.remove();
    return false;
}
document.getElementById('form_delete').addEventListener('click', function (e) {
    e.preventDefault();
})

document.getElementById('submit').addEventListener('click', function (e) {
    if (document.querySelector('.input_parent').value == '' && document.querySelector('.input_enfant').value == '') { e.preventDefault(); }
    let add_champ_name = document.querySelectorAll('.add_champ_name');
    add_champ_name.forEach(el => {
        if (el.value == '') {
            e.preventDefault();
        }
    })
})
insert_image();
//add image multiple
var index_image;
function insert_image()
{
    $(function () {
    $('#images-' + i).change(function (e) {
         index_image = e.target.id
      var files = e.target.files;
        for (var k = 0; k <= files.length; k++) {
            
            if (k == files.length) {
        setTimeout(function(){ reorderImages(); }, 100);
        break;
      }

      var file = files[k];
      var reader = new FileReader();
            reader.onload = (function (item) {
                return function (event) {
                    console.log(item);
        $('#sortable-' + index_image.split('-')[1]).prepend('<div class="ui-state-default"  data-id="' + item.lastModified + '"><img class="list_edit_image"  src="' + event.target.result + '" /><span class="remove_icon_image"><svg width="10px" height="10px"  onclick="removeaddimage('+item.lastModified+','+index_image.split('-')[1]+' )" class="times" viewBox="0 0 24 24"> <path fill="currentColor" d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" /> </svg></span></div>');
        };
      })(file);
      
      reader.readAsDataURL(file);
    }// end for;
    
  });
  
//   $('#sortable').sortable();
//   $('#sortable').disableSelection();
  
  //sortable events
  $('#sortable').on('sortbeforestop',function(event){
    reorderImages();
  });
  
  
  function reorderImages(){
    // get the items
    var images = $('#sortable');
    
    var j=0, nrOrder=0;
    for(j;j<images.length;j++){
        var image = $(images[j]);
      if( image.hasClass('ui-state-default') && !image.hasClass('ui-sortable-placeholder') ){
        image.attr('data-order', nrOrder);
        var orderNumberBox = image.find('.order-number');
        orderNumberBox.html(nrOrder+1);
        nrOrder++;
      }// end if;
      
    }// end for;
  }
   
 
});
}
function removeaddimage(id,place) {
    var a = document.querySelector('div[data-id= "' + id + '"]');
    a.parentNode.removeChild(a);
    // removeFile(id);
    fileremove(id, place)
    
}
// function removeFile(lastModified) {
//     console.log(j);
//     console.log('removefile');
//     var attachments = document.getElementById("images-1").files; // <-- reference your file input here
//     console.log('value input values',attachments);
//     var fileBuffer = new DataTransfer();
//     var id=Array.from(attachments).findIndex(element => element.lastModified === lastModified)

//     // append the file list to an array iteratively
//     for (let i = 0; i < attachments.length; i++) {
//         // Exclude file in specified index
//         //console.log(attachments[i])
//         if (id !== i)
//             fileBuffer.items.add(attachments[i]);
//     }
    
//     // Assign buffer to file input
//     document.getElementById("images-1").files = fileBuffer.files; // <-- according to your file input reference
// }

function fileremove(lastModified = null, j = null)
{
    // console.log('places', j);
    // let inputs=document.querySelectorAll('input[type="file"]');
    // console.log(inputs);
    // inputs.forEach((el, index) => {
    //     console.log(index);
    //     if (el!="pdf")
    //     {
    //         if (el.id.split('-')[1])
    //             if ( j == el.id.split('-')[1])   
    //             {
    //               attachments(lastModified,j)
    //             }
    //     }   

    // })
    attachments(lastModified,j);
}
function attachments(lastModified,j)
{
    console.log(lastModified);
     var attachments = document.getElementById("images-"+j).files; // <-- reference your file input here
        console.log(attachments);
        var fileBuffer = new DataTransfer();
        var id=Array.from(attachments).findIndex(element => element.lastModified === lastModified)

        // append the file list to an array iteratively
        for (let i = 0; i < attachments.length; i++) {
            // Exclude file in specified index
            //console.log(attachments[i])
            if (id !== i)
                fileBuffer.items.add(attachments[i]);
        }
        
        // Assign buffer to file input
        document.getElementById("images-1").files = fileBuffer.files; // <-- according to your file input reference

}