/*  Delete Variant */
function deletevariant(id, product_id)
{
    $.ajax({
        url: "/admin/variant/destroy/"+id,
        type: "DELETE",
        data: {
            _method: 'DELETE',
            _token: $('meta[name="csrf-token"]').attr('content'),
        },
        success: function(data) {
            window.location.href = "/admin/products/"+product_id+"/details";
        }
    });
}

var compteur;
var index_file_default;

/*Edit variant */
function edit_variant(item, product_id)
{
    console.log(item);
    let name_parent= document.querySelector('.name_parent');
    name_parent.value=item.name
    name_parent.id=item.id

    let product_variant_id= document.querySelector('#product_variant_id');
    product_variant_id.value = item.id;

    let product_Id= document.querySelector('#product_Id');
    product_Id.value = product_id;

    var TabInput = [];
  
    // form(item.product_variant_values.length + 1);  
    compteur = item.product_variant_values.length;
    item.product_variant_values.forEach((element, index) => {
        index_file_default = index;
    upload_update_image(index + 1);
    htmldefault(element, index)
    TabInput.push(htmldefault(element, index));
    })

    function defaultform() {
        TabInput.forEach(el => {
            $('#variant_value_name').append(el)
        })
    }
    defaultform();
}

/*Formulaire edit par defaut*/
function htmldefault(el, i)
{
            var img = ``;
            el.media.forEach((item, index) => {
                img += `
                <div class="delete_image-${i}-${index}">
                <input type="hidden" id="images-${i+1}"  name="variant_value[${i}][file][]" accept="image/*" value="${item.file}"  style="visibility: hidden; position: absolute;" multiple/>                        
                <img alt="" class="list_edit_image" src="${item.file}"/> 
                 <span>
                    <svg onclick="removeimage(${i},${index})" class="times remove_icon" viewBox="0 0 24 24">
                        <path fill="currentColor" d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" />
                    </svg>
                </span>
                </div>
                `           
                });
            return `<div>
            <div class="row" del="${i}">
                            <div class="col-md-4">
                                <label for="name"><b>Nom *</b></label>
                                <input required="required" class="form-control input_enfant" id="value" value="${el.value}" name="variant_value[${i}][value]" type="text" placeholder="L,XL,XXL,.." />
                            </div>
                            
                            <div class="col-md-4">
                                <label for="name"><b>Quantité</b></label>
                                <input  class="form-control input_enfant" id="amount" value="${el.amount}" name="variant_value[${i}][amount]" type="number" min="0" placeholder="Quantite" />
                            </div>

                            <div class="col-md-4">
                                    <label for="capatity"><b>Capacite </b></label>
                                <input  class="form-control input_enfant" id="capacity" value="${el.capacity}" name="variant_value[${i}][capacity]" type="text" placeholder="Capacite"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 mt-3">
                                <label for="price">
                                    <b>PGHT</b>
                                    <svg xmlns="http://www.w3.org/2000/svg" style="cursor: pointer" data-animation="true" data-html="true" data-toggle="tooltip" data-placement="top" title="
                                    Prix de gros HT du produit
                                    " width="16px" height="16px" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 48 48">
                                        <path fill="#8f8f8f" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"/><path fill="#fff" d="M22 22h4v11h-4V22zM26.5 16.5c0 1.379-1.121 2.5-2.5 2.5s-2.5-1.121-2.5-2.5S22.621 14 24 14 26.5 15.121 26.5 16.5z"/>
                                    </svg>
                                </label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend"><span class="input-group-text" >€</span></div>
                                    <input class="form-control" value="${el.price_pght}" name="variant_value[${i}][price_pght]"  id="price_pght" type="text" placeholder="Prix" aria-label="price" aria-describedby="price" required />
                                </div>     
                            </div>
                            
                            <div class="col-md-4 mt-3">
                                <label for="price">
                                    <b>PPI</b>
                                     <svg xmlns="http://www.w3.org/2000/svg" style="cursor: pointer" data-animation="true" data-html="true" data-toggle="tooltip" data-placement="top" title="
                                    Prix public indicatif unitaire
                                    " width="16px" height="16px" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 48 48">
                                        <path fill="#8f8f8f" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"/><path fill="#fff" d="M22 22h4v11h-4V22zM26.5 16.5c0 1.379-1.121 2.5-2.5 2.5s-2.5-1.121-2.5-2.5S22.621 14 24 14 26.5 15.121 26.5 16.5z"/>
                                    </svg>
                                </label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend"><span class="input-group-text" >€</span></div>
                                    <input class="form-control" name="variant_value[${i}][price_ppi]" value="${el.price_ppi}" id="price_ppi" type="text" placeholder="Prix" aria-label="price_ppi" aria-describedby="price_ppi" required />
                                </div>        
                            </div>

                            <div class="col-md-4 mt-3">
                                <label for="name"><b>Code</b></label>
                                <div class="input-group mb-3">
                                    <input class="form-control" name="variant_value[${i}][coded]" value="${el.coded}" id="coded" type="text" maxlength="13" placeholder="code" aria-label="coded" aria-describedby="coded" required />
                                </div> 
                            </div>
                        </div>
                            <div class="row">
                            <div class="col-md-4 mt-3">
                                <label for="name"><b>Largeur
                                 <svg xmlns="http://www.w3.org/2000/svg" style="cursor: pointer" data-animation="true" data-html="true" data-toggle="tooltip" data-placement="top" title="
                                    Largeur
                                    " width="16px" height="16px" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 48 48">
                                        <path fill="#8f8f8f" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"/><path fill="#fff" d="M22 22h4v11h-4V22zM26.5 16.5c0 1.379-1.121 2.5-2.5 2.5s-2.5-1.121-2.5-2.5S22.621 14 24 14 26.5 15.121 26.5 16.5z"/>
                                    </svg>
                                </b>
                                 <svg xmlns="http://www.w3.org/2000/svg" style="cursor: pointer" data-animation="true" data-html="true" data-toggle="tooltip" data-placement="top" title="
                                    largeur
                                    " width="16px" height="16px" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 48 48">
                                        <path fill="#8f8f8f" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"/><path fill="#fff" d="M22 22h4v11h-4V22zM26.5 16.5c0 1.379-1.121 2.5-2.5 2.5s-2.5-1.121-2.5-2.5S22.621 14 24 14 26.5 15.121 26.5 16.5z"/>
                                    </svg>
                                </label>
                                <input  class="form-control input_enfant" id="weight" name="variant_value[${i}][weight]" value="${el.weight}" type="number" min="0" placeholder="Largeur" />
                            </div>
                            <div class="col-md-4 mt-3">
                                <label for="name"><b>Dimension </b>
                                 <svg xmlns="http://www.w3.org/2000/svg" style="cursor: pointer" data-animation="true" data-html="true" data-toggle="tooltip" data-placement="top" title="
                                   Dimension
                                    " width="16px" height="16px" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 48 48">
                                        <path fill="#8f8f8f" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"/><path fill="#fff" d="M22 22h4v11h-4V22zM26.5 16.5c0 1.379-1.121 2.5-2.5 2.5s-2.5-1.121-2.5-2.5S22.621 14 24 14 26.5 15.121 26.5 16.5z"/>
                                    </svg>
                                </label>
                                <input  class="form-control input_enfant" id="dimension" name="variant_value[${i}][dimension]" value="${el.dimension}" type="number" min="0" placeholder="dimension" />
                            </div>
                            <div class="col-md-4 mt-3">
                                <label for="images-${i + 1}"> Ajouter Image</label>
                                <input type="hidden" id="images-${i + 1}"  name="variant_value[${i}][file][]" accept="image/*"   style="visibility: hidden; position: absolute;" multiple/> 
                            </div>
                            </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="multiple-file-preview">
                                    <div id="edit-image-${i + 1}" class="list_img mt-1 d-flex">
                                        <div class="clear-both mt-3  d-flex">
                                            ${img}
                                        </div>    
                                    </div>
                                </div>   
                            </div>
                        </div>
                        <div class="col-md-12 mt-3 mb-2">
                            <div class="d-flex justify-content-end">
                                <button class="btn btn-primary ml-2 addboutton" onclick="return addInput(this)">
                                    +
                                </button>
                                <button class="btn btn-danger ml-2" onclick="clearInput(this)" id="form_delete">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                        <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                    </svg>
                                </button>
                            </div>
                        </div>    
    </div> 
    `                
}



/*Adjouter form variant */
let TABADD=[];
function addform() {
    TABADD.forEach(el => {
        $('#variant_value_name').append(el)
    })
}
        // var champ = `
        //         <div class="champ_name">
        //             <div class="col-md-12 mb-3"  >
        //                 <label for="value" ><b>Nom *</b></label>
        //                 <div class="d-flex" del="" >
        //                 <input class="form-control name_edit" id="value" name="value[]" type="text" placeholder="L,XL,XXL,.."/>
        //                 <button class="btn btn-primary  ml-1 addbutton" onclick="addInput()">+</button>
        //                     <button class="btn btn-danger ml-2"
        //                         onclick="clearInput(this)">
        //                         <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
        //                             <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
        //                         <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
        //                         </svg>
        //                     </button>
        //                 </div>
        //             </div>
        //         </div>`;

addform();



/*Formulaire ajouter*/
function form(i)
{
            // return    `
            //   @method('PUT')
            // <div class="champ_name" id="clear">
            //     <div class="col-md-12 mb-3">
            //         <label for="name"><b>Nom *</b></label>
            //         <div class="d-flex " del="${index}" >
            //             <input class="form-control name_edit" value="${element.value}" id="value[]"   name="value[]" type="text" placeholder="L,XL,XXL,.." />
            //             <button class="btn btn-primary addboutton ml-1" onclick="addInput() ">+</button>
            //             <button class="btn btn-danger ml-2 " onclick=" clearInput(this)"> <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
            //                     <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
            //                     <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
            //                 </svg>
            //             </button>
            //         </div>
            //     </div>
            // </div>`;

                    return `<div>
            <div class="row" del="${i}">
                            <div class="col-md-4">
                                <label for="name"><b>Nom *</b></label>
                                <input required="required" class="form-control input_enfant" id="value" name="variant_value[${i}][value]" type="text" placeholder="L,XL,XXL,.." />
                            </div>
                            
                            <div class="col-md-4">
                                <label for="name"><b>Quantité</b></label>
                                <input  class="form-control input_enfant" id="amount" name="variant_value[${i}][amount]" type="number" min="0" placeholder="Quantite" />
                            </div>

                            <div class="col-md-4">
                                    <label for="capatity"><b>Capacite </b></label>
                                <input  class="form-control input_enfant" id="capacity" name="variant_value[${i}][capacity]" type="text" placeholder="Capacite"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 mt-3">
                                <label for="price">
                                    <b>PGHT</b>
                                    <svg xmlns="http://www.w3.org/2000/svg" style="cursor: pointer" data-animation="true" data-html="true" data-toggle="tooltip" data-placement="top" title="
                                    Prix de gros HT du produit
                                    " width="16px" height="16px" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 48 48">
                                        <path fill="#8f8f8f" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"/><path fill="#fff" d="M22 22h4v11h-4V22zM26.5 16.5c0 1.379-1.121 2.5-2.5 2.5s-2.5-1.121-2.5-2.5S22.621 14 24 14 26.5 15.121 26.5 16.5z"/>
                                    </svg>
                                </label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend"><span class="input-group-text" >€</span></div>
                                    <input class="form-control" name="variant_value[${i}][price_pght]" value="" id="price_pght" type="text" placeholder="Prix" aria-label="price" aria-describedby="price" required />
                                </div>     
                            </div>
                            
                            <div class="col-md-4 mt-3">
                                <label for="price">
                                    <b>PPI</b>
                                    <svg xmlns="http://www.w3.org/2000/svg" style="cursor: pointer" data-animation="true" data-html="true" data-toggle="tooltip" data-placement="top" title="
                                    Prix de gros HT du produit
                                    " width="16px" height="16px" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 48 48">
                                        <path fill="#8f8f8f" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"/><path fill="#fff" d="M22 22h4v11h-4V22zM26.5 16.5c0 1.379-1.121 2.5-2.5 2.5s-2.5-1.121-2.5-2.5S22.621 14 24 14 26.5 15.121 26.5 16.5z"/>
                                    </svg>
                                </label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend"><span class="input-group-text" >€</span></div>
                                    <input class="form-control" name="variant_value[${i}][price_ppi]" value="" id="price_ppi" type="text" placeholder="Prix" aria-label="price_ppi" aria-describedby="price_ppi" required />
                                </div>        
                            </div>

                            <div class="col-md-4 mt-3">
                                <label for="name"><b>Code</b></label>
                                <div class="input-group mb-3">
                                    <input class="form-control" name="variant_value[${i}][coded]" value="coded" id="coded" type="text" maxlength="13" placeholder="code" aria-label="coded" aria-describedby="coded" required />
                                </div> 
                            </div>
                        </div>
                            <div class="row">
                            <div class="col-md-4 mt-3">
                                <label for="name"><b>Largeur</b></label>
                                <input  class="form-control input_enfant" id="weight" name="variant_value[${i}][weight]" type="number" min="0" placeholder="Largeur" />
                            </div>
                            <div class="col-md-4 mt-3">
                                <label for="name"><b>Demension</b></label>
                                <input  class="form-control input_enfant" id="dimension" name="variant_value[${i}][dimension]" type="number" min="0" placeholder="dimension" />
                            </div>
                            <div class="col-md-4 mt-3">
                                <label for="images-${i+1}"> Upload Image</label>
                                <input type="file" id="images-${i+1}"  name="variant_value[${i}][file][]" accept="image/*"  style="visibility: hidden; position: absolute;" multiple/>    
                            </div>
                            </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="multiple-file-preview">
                                <div id="edit-image-${i+1}" class="list_img d-flex mt-1">
                                    <div class="clear-both">
                                    </div>
                                </div>
                                </div>   
                            </div>
                        </div>
                        <div class="col-md-12 mt-3 mb-2">
                            <div class="d-flex justify-content-end">
                                <button class="btn btn-primary ml-2 addboutton" onclick="return addInput(this)">
                                    +
                                </button>
                                <button class="btn btn-danger ml-2" onclick="DELETEINPUT(this)" id="form_delete">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                        <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                    </svg>
                                </button>
                            </div>
                        </div>    
    </div> 
    `
                
}


       
/*Fonction ajouter*/
function addInput() {
    // document.querySelector("#formulaire_edit").addEventListener("click", function(event) {
    //     event.preventDefault();
    // }, false);
    TABADD.push(form(compteur));
    addform();
    TABADD.shift(form(compteur));
    compteur++;
    upload_update_image(compteur);
     return false;
}

function clearInput(obj) {
    document.querySelector("#formulaire_edit").addEventListener("click", function(event) {
        event.preventDefault();
    }, false);
    let champs_names=document.querySelectorAll('div[del]');
    if(champs_names.length<=1)
    {
        return;
    }
    obj.parentElement.parentElement.parentElement.remove();
}


function upload_update_image(i)
{
    $(function () {
    $('#images-' + i).change(function (e) {
        var index_image = e.target.id
      var files = e.target.files;
		for (var k = 0; k <= files.length; k++) {
      if(k==files.length){
        setTimeout(function(){ reorderImages(); }, 100);
        break;
      }

      var file = files[k];
      var reader = new FileReader();
            reader.onload = (function (item) {
            return function (event) {
                $('#edit-image-' + index_image.split('-')[1]).prepend('<div class="ui-state-default mt-3" data-order=0 data-id="' + item.lastModified + '"><img class="list_edit_image"  src="' + event.target.result + '" /><span onclick="removeimageadd('+item.lastModified+')" class="remove_icon_image"><svg width="10px" height="10px"  class="times" viewBox="0 0 24 24"> <path fill="currentColor" d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" /> </svg></span></div>');
        };
      })(file);
      
      reader.readAsDataURL(file);
    }// end for;
    
  });
  
//   $('#sortable').sortable();
//   $('#sortable').disableSelection();
  
  //sortable events
  $('#sortable').on('sortbeforestop',function(event){
    reorderImages();
  });
  
  
  function reorderImages(){
    // get the items
    var images = $('#sortable');
    
    var j=0, nrOrder=0;
    for(j;j<images.length;j++){
      var image = $(images[j]);
      if( image.hasClass('ui-state-default') && !image.hasClass('ui-sortable-placeholder') ){
        image.attr('data-order', nrOrder);
        var orderNumberBox = image.find('.order-number');
        orderNumberBox.html(nrOrder+1);
        nrOrder++;
      }// end if;
      
    }// end for;
  } 
});
}
function removeimage(parent,enfant)
{
    let list_img=document.querySelector('.delete_image-'+parent+'-'+enfant);
    list_img.parentNode.removeChild(list_img);
}
function removeimageadd(id)
{
    var data_id = document.querySelector('div[data-id= "' + id + '"]');
    data_id.parentNode.removeChild(data_id);

    // let input = document.getElementById('images-' + i);
    // input.parentNode.removeChild(input);

    // let image = document.getElementById('edit-image-' + i);
    // console.log(image);
    // image.parentElement.remove()
    removeFile(id);
}

function removeFile(lastModified){
    var attachments = document.getElementById("images-1").files; // <-- reference your file input here
    var fileBuffer = new DataTransfer();
    var id=Array.from(attachments).findIndex(element => element.lastModified === lastModified)

    // append the file list to an array iteratively
    for (let i = 0; i < attachments.length; i++) {
        // Exclude file in specified index
        //console.log(attachments[i])
        if (id !== i)
            fileBuffer.items.add(attachments[i]);
    }
    
    // Assign buffer to file input
    document.getElementById("images-1").files = fileBuffer.files; // <-- according to your file input reference
}



       
